package com.gzemlyakov.ig.trader.core.model;

public enum PositionDirection {
        BUY,
        SELL
    }
