package com.gzemlyakov.ig.trader.core;

import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketObserver;
import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItem;
import com.gzemlyakov.ig.trader.core.model.order.OrderItem;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionRequest;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionResponse;

import java.util.List;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface TraderFacade {

    void connect(String logIn, String password, String apiKey) throws Exception;

    List<MarketItem> initForexMarket() throws Exception;

    void disconnect() throws Exception;

    void registerMarketObserver(MarketObserver marketObserver);

/*    List<PriceItem> requestMarketItemHistory(String epic, String resolution,
                                             ZonedDateTime startDate, ZonedDateTime endDate) throws Exception;*/

    List<PriceItem> requestMarketItemHistory(String epic, String resolution, int numberOfPoints) throws Exception;

    CreatePositionResponse createPosition(CreatePositionRequest createPositionRequest) throws Exception;

    List<OrderItem> getWorkingOrders() throws Exception;
}
