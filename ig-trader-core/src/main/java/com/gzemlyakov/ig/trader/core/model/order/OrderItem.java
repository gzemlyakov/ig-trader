package com.gzemlyakov.ig.trader.core.model.order;

import java.math.BigDecimal;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class OrderItem {

    private String symbol;
    private BigDecimal bid;
    private String createdDate;
    private String direction;
    private String timeInForce;
    private String goodTillDate;
    private Boolean guaranteedStop;
    private BigDecimal limitDistance;
    private BigDecimal stopDistance;
    private BigDecimal orderLevel;
    private Float size;
    private String orderType;

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setBid(BigDecimal bid) {
        this.bid = bid;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce = timeInForce;
    }

    public void setGoodTillDate(String goodTillDate) {
        this.goodTillDate = goodTillDate;
    }

    public void setGuaranteedStop(Boolean guaranteedStop) {
        this.guaranteedStop = guaranteedStop;
    }

    public void setLimitDistance(BigDecimal limitDistance) {
        this.limitDistance = limitDistance;
    }

    public void setStopDistance(BigDecimal stopDistance) {
        this.stopDistance = stopDistance;
    }

    public void setOrderLevel(BigDecimal orderLevel) {
        this.orderLevel = orderLevel;
    }

    public void setSize(Float size) {
        this.size = size;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSymbol() {
        return symbol;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getDirection() {
        return direction;
    }

    public String getTimeInForce() {
        return timeInForce;
    }

    public String getGoodTillDate() {
        return goodTillDate;
    }

    public Boolean getGuaranteedStop() {
        return guaranteedStop;
    }

    public BigDecimal getLimitDistance() {
        return limitDistance;
    }

    public BigDecimal getStopDistance() {
        return stopDistance;
    }

    public BigDecimal getOrderLevel() {
        return orderLevel;
    }

    public Float getSize() {
        return size;
    }

    public String getOrderType() {
        return orderType;
    }
}
