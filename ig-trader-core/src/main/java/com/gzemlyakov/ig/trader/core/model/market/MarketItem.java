package com.gzemlyakov.ig.trader.core.model.market;

import java.util.List;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class MarketItem {

    private String epic;
    private String expiry;
    private String symbol;
    private List<String> currencies;
    private double minNormalStopOrLimitDistance;
    private int scalingFactor;
    private double minControlledRiskStopDistance;

    public String getEpic() {
        return epic;
    }

    public void setEpic(String epic) {
        this.epic = epic;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

    public double getMinNormalStopOrLimitDistance() {
        return minNormalStopOrLimitDistance;
    }

    public void setMinNormalStopOrLimitDistance(double minNormalStopOrLimitDistance) {
        this.minNormalStopOrLimitDistance = minNormalStopOrLimitDistance;
    }

    public int getScalingFactor() {
        return scalingFactor;
    }

    public void setScalingFactor(int scalingFactor) {
        this.scalingFactor = scalingFactor;
    }

    public double getMinControlledRiskStopDistance() {
        return minControlledRiskStopDistance;
    }

    public void setMinControlledRiskStopDistance(double minControlledRiskStopDistance) {
        this.minControlledRiskStopDistance = minControlledRiskStopDistance;
    }
}
