package com.gzemlyakov.ig.trader.core.model;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public enum CurrencyCode {

    ;

    private String currencyCode;

    CurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return currencyCode;
    }

}
