package com.gzemlyakov.ig.trader.core.model.market;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class MarketItemUpdateInfo {

    private String epic;
    private boolean isSellPriceChanged;
    private boolean isBuyPriceChanged;
    private ValueItem buyValue;
    private ValueItem sellValue;

    public MarketItemUpdateInfo(String epic, boolean isSellPriceChanged, boolean isBuyPriceChanged,
                                ValueItem sellValue, ValueItem buyValue) {
        this.epic = epic;
        this.isSellPriceChanged = isSellPriceChanged;
        this.isBuyPriceChanged = isBuyPriceChanged;
        this.sellValue = sellValue;
        this.buyValue = buyValue;
    }

    public String getEpic() {
        return epic;
    }

    public void setEpic(String epic) {
        this.epic = epic;
    }

    public boolean isSellPriceChanged() {
        return isSellPriceChanged;
    }

    public void setSellPriceChanged(boolean sellPriceChanged) {
        isSellPriceChanged = sellPriceChanged;
    }

    public boolean isBuyPriceChanged() {
        return isBuyPriceChanged;
    }

    public void setBuyPriceChanged(boolean buyPriceChanged) {
        isBuyPriceChanged = buyPriceChanged;
    }

    public ValueItem getBuyValue() {
        return buyValue;
    }

    public void setBuyValue(ValueItem buyValue) {
        this.buyValue = buyValue;
    }

    public ValueItem getSellValue() {
        return sellValue;
    }

    public void setSellValue(ValueItem sellValue) {
        this.sellValue = sellValue;
    }

}
