package com.gzemlyakov.ig.trader.core.model.position;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class CreatePositionResponse {

    private Status status;
    private String reason;

    public CreatePositionResponse(Status status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public enum Status {
        ACCEPTED,
        FUND_ACCOUNT,
        REJECTED
    }

}
