package com.gzemlyakov.ig.trader.core.iggroup.data.market;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface MarketManager {

    void register(MarketObserver marketObserver);

    void remove(MarketObserver marketObserver);

}
