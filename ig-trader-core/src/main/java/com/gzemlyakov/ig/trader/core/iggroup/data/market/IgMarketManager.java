package com.gzemlyakov.ig.trader.core.iggroup.data.market;

import com.gzemlyakov.ig.trader.core.iggroup.data.market.converter.IgForexMarketDataConverter;
import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;
import com.iggroup.api.markets.navigation.getMarketNavigationNodeV1.MarketsItem;
import com.iggroup.api.streaming.HandyTableListenerAdapter;
import com.lightstreamer.ls_client.UpdateInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class IgMarketManager extends HandyTableListenerAdapter implements MarketManager {

    private final Logger logger = LoggerFactory.getLogger(IgMarketManager.class);

    @Value("${ig.api.forex.default.expiry:-}")
    private String defaultExpiry;

    @Autowired
    IgForexMarketDataConverter igForexMarketDataConverter;

    private List<MarketObserver> marketListeners;

    public IgMarketManager() {
        marketListeners = new ArrayList<>();
    }

    @Override
    public void onUpdate(int i, String s, UpdateInfo updateInfo) {
        if (CollectionUtils.isEmpty(marketListeners)) return;
        try {
            MarketItemUpdateInfo marketItemUpdateInfo = igForexMarketDataConverter.convertItemUpdateInfo(updateInfo);
            for (MarketObserver marketObserver : marketListeners)
                marketObserver.onUpdate(marketItemUpdateInfo);
        } catch (Exception e) {
            logger.error("Unable to convert update info", e);
        }
    }

    @Override
    public void register(MarketObserver marketObserver) {
        marketListeners.add(marketObserver);
    }

    @Override
    public void remove(MarketObserver marketObserver) {
        marketListeners.remove(marketObserver);
    }

}
