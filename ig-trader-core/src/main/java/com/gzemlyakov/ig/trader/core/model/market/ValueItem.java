package com.gzemlyakov.ig.trader.core.model.market;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class ValueItem {

    private double oldValue;
    private double newValue;

    public ValueItem(double oldValue, double newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public double getOldValue() {
        return oldValue;
    }

    public void setOldValue(double oldValue) {
        this.oldValue = oldValue;
    }

    public double getNewValue() {
        return newValue;
    }

    public void setNewValue(double newValue) {
        this.newValue = newValue;
    }

    @Override
    public String toString() {
        return String.valueOf(newValue);
    }

}
