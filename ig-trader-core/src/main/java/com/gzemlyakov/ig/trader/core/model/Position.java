package com.gzemlyakov.ig.trader.core.model;

import java.math.BigDecimal;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class Position {

    private PositionDirection positionDirection;
    private CurrencyCode currencyCode;
    private BigDecimal size;

    public Position(PositionDirection positionDirection, CurrencyCode currencyCode, BigDecimal size) {
        this.positionDirection = positionDirection;
        this.currencyCode = currencyCode;
        this.size = size;
    }

    public PositionDirection getPositionDirection() {
        return positionDirection;
    }

    public void setPositionDirection(PositionDirection positionDirection) {
        this.positionDirection = positionDirection;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

}
