package com.gzemlyakov.ig.trader.core.iggroup.data.market.converter;

import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;
import com.gzemlyakov.ig.trader.core.model.order.OrderItem;
import com.gzemlyakov.ig.trader.core.model.position.WorkingOrder;
import com.iggroup.api.prices.getPricesByNumberOfPointsV2.PricesItem;
import com.iggroup.api.workingorders.getWorkingOrdersV2.GetWorkingOrdersV2Response;
import com.iggroup.api.workingorders.getWorkingOrdersV2.WorkingOrdersItem;
import com.lightstreamer.ls_client.UpdateInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface IgForexMarketDataConverter {

/*    void init(Map<String, String> symbols);*/

    MarketItemUpdateInfo convertItemUpdateInfo(UpdateInfo updateInfo) throws Exception;

    List<PriceItem> convertPriceItems(List<PricesItem> priceItems);

    PriceItem convertPriceItem(PricesItem pricesItem);

    List<MarketItem> convertMarketItems();

    List<WorkingOrder> convertOrderItems(GetWorkingOrdersV2Response workingOrdersResponse);

    List<OrderItem> convertOrderItems(List<WorkingOrdersItem> workingOrdersItems);
}
