package com.gzemlyakov.ig.trader.core.model.position;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class WorkingOrder {

    private String instrumentName;
    private ZonedDateTime createdDate;
    private String currencyCode;
    private String direction;
    private ZonedDateTime goodTillDate;
    private BigDecimal limitDistance;
    private BigDecimal orderLevel;
    private BigDecimal orderSize;
    private String orderType;
    private BigDecimal stopDistance;

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public ZonedDateTime getGoodTillDate() {
        return goodTillDate;
    }

    public void setGoodTillDate(ZonedDateTime goodTillDate) {
        this.goodTillDate = goodTillDate;
    }

    public BigDecimal getLimitDistance() {
        return limitDistance;
    }

    public void setLimitDistance(BigDecimal limitDistance) {
        this.limitDistance = limitDistance;
    }

    public BigDecimal getOrderLevel() {
        return orderLevel;
    }

    public void setOrderLevel(BigDecimal orderLevel) {
        this.orderLevel = orderLevel;
    }

    public BigDecimal getOrderSize() {
        return orderSize;
    }

    public void setOrderSize(BigDecimal orderSize) {
        this.orderSize = orderSize;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getStopDistance() {
        return stopDistance;
    }

    public void setStopDistance(BigDecimal stopDistance) {
        this.stopDistance = stopDistance;
    }
}
