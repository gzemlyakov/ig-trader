package com.gzemlyakov.ig.trader.core.iggroup.facade;

import com.gzemlyakov.ig.trader.core.TraderFacade;
import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketManager;
import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketObserver;
import com.gzemlyakov.ig.trader.core.iggroup.data.market.converter.IgForexMarketDataConverter;
import com.gzemlyakov.ig.trader.core.iggroup.subscription.LightStreamerDecorator;
import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItem;
import com.gzemlyakov.ig.trader.core.model.order.OrderItem;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionRequest;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionResponse;
import com.iggroup.api.API;
import com.iggroup.api.confirms.getDealConfirmationV1.GetDealConfirmationV1Response;
import com.iggroup.api.markets.getMarketDetailsListV2.CurrenciesItem;
import com.iggroup.api.markets.getMarketDetailsListV2.GetMarketDetailsListV2Response;
import com.iggroup.api.markets.getMarketDetailsListV2.MarketDetailsItem;
import com.iggroup.api.markets.navigation.getMarketNavigationNodeV1.GetMarketNavigationNodeV1Response;
import com.iggroup.api.markets.navigation.getMarketNavigationNodeV1.MarketsItem;
import com.iggroup.api.prices.getPricesByNumberOfPointsV2.GetPricesByNumberOfPointsV2Response;
import com.iggroup.api.service.AuthenticationResponseAndConversationContext;
import com.iggroup.api.session.createSessionV2.CreateSessionV2Request;
import com.iggroup.api.streaming.HandyTableListenerAdapter;
import com.iggroup.api.workingorders.getWorkingOrdersV2.GetWorkingOrdersV2Response;
import com.iggroup.api.workingorders.otc.createOTCWorkingOrderV2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class IgTraderFacade implements TraderFacade {

    private static final double PERCENT = 100;

    private final Logger logger = LoggerFactory.getLogger(IgTraderFacade.class);

    @Autowired
    private API api;

    @Autowired
    private LightStreamerDecorator lightStreamerComponent;

    @Autowired
    IgForexMarketDataConverter igForexMarketDataConverter;

    @Autowired
    MarketManager marketManager;

    @Value("${ig.api.forex.market.id:264134}")
    private String marketId;

    @Value("${ig.api.date.time.pattern:yyyy-MM-dd'T'HH:mm:ss}")
    private String dateTimePattern;

    private AuthenticationResponseAndConversationContext authenticationContext;

    @Override
    public void connect(String logIn, String password, String apiKey) throws Exception {
        CreateSessionV2Request authRequest = new CreateSessionV2Request();
        authRequest.setIdentifier(logIn);
        authRequest.setPassword(password);
        authenticationContext = api.createSession(authRequest, apiKey);

        lightStreamerComponent.connect(authenticationContext.getCreateSessionResponse().getCurrentAccountId(),
                authenticationContext.getConversationContext(),
                authenticationContext.getCreateSessionResponse().getLightstreamerEndpoint());
    }

    @Override
    public List<MarketItem> initForexMarket() throws Exception {//TODO Move response parsing to another entity
        GetMarketNavigationNodeV1Response marketResponse =
                api.getMarketNavigationNodeV1(authenticationContext.getConversationContext(), marketId);
        GetMarketDetailsListV2Response marketDetailsResponse = api.getMarketDetailsListV2(authenticationContext.getConversationContext(),
                marketResponse.getMarkets().stream().map(MarketsItem::getEpic).collect(Collectors.joining(",")));
        List<MarketItem> marketItems = new ArrayList<>();
        for (int i = 0; i < marketResponse.getMarkets().size(); i++) {
            MarketsItem market = marketResponse.getMarkets().get(i);
            MarketDetailsItem marketDetails = marketDetailsResponse.getMarketDetails().get(i);
            if (market.getEpic().equals(marketDetails.getInstrument().getEpic()))
                logger.warn("Market items order differs in GetMarketNavigationNodeV1Response and GetMarketDetailsListV2Response");
            MarketItem marketItem = new MarketItem();
            marketItem.setEpic(market.getEpic());
            marketItem.setExpiry(market.getExpiry());
            marketItem.setSymbol(market.getInstrumentName());
            marketItem.setCurrencies(marketDetails.getInstrument().getCurrencies().stream().map(CurrenciesItem::getCode)
                    .collect(Collectors.toList()));
            marketItem.setMinNormalStopOrLimitDistance(
                    marketDetails.getDealingRules().getMinNormalStopOrLimitDistance().getValue());
            marketItem.setScalingFactor(market.getScalingFactor());
            marketItem.setMinControlledRiskStopDistance(marketDetails.getDealingRules().getMinControlledRiskStopDistance().getValue() / PERCENT); //Divide by 100 to calculate percent
            marketItems.add(marketItem);
            lightStreamerComponent.subscribeForMarket(market.getEpic(), (HandyTableListenerAdapter) marketManager);
        }
        return marketItems;
    }

    @Override
    public void disconnect() throws Exception {
        lightStreamerComponent.unsubscribeAll();
        lightStreamerComponent.disconnect();
    }

    @Override
    public void registerMarketObserver(MarketObserver marketObserver) {
        marketManager.register(marketObserver);
    }

/*    @Override
    public List<PriceItem> requestMarketItemHistory(String epic, String resolution, ZonedDateTime startDate, ZonedDateTime endDate) throws Exception {
        if (dateTimeFormatter == null) dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);
        GetPricesByDateRangeV2Response priceHistory
                = api.getPricesByDateRangeV2(authenticationContext.getConversationContext(), epic, resolution,
                startDate.format(dateTimeFormatter), endDate.format(dateTimeFormatter));
        return igForexMarketDataConverter.convertPriceItems(priceHistory.getPrices());
    }*/

    @Override
    public List<PriceItem> requestMarketItemHistory(String epic, String resolution, int numberOfPoints) throws Exception {
        GetPricesByNumberOfPointsV2Response priceHistory = api.getPricesByNumberOfPointsV2(
                authenticationContext.getConversationContext(), epic, resolution, String.valueOf(numberOfPoints));
        priceHistory.getPrices();
        return igForexMarketDataConverter.convertPriceItems(priceHistory.getPrices());
    }

    @Override
    public CreatePositionResponse createPosition(CreatePositionRequest createPositionRequest) throws Exception {
        CreateOTCWorkingOrderV2Request createPosition = new CreateOTCWorkingOrderV2Request();
        createPosition.setCurrencyCode(createPositionRequest.getCurrencyCode());
        createPosition.setDirection(Direction.valueOf(createPositionRequest.getDirection().name()));
        createPosition.setEpic(createPositionRequest.getEpic());
        createPosition.setExpiry(createPositionRequest.getExpiry());
        createPosition.setForceOpen(createPositionRequest.isForceOpen());
        createPosition.setGoodTillDate(createPositionRequest.getGoodTillDate());
        createPosition.setGuaranteedStop(createPositionRequest.isGuaranteedStop());
        createPosition.setLevel(createPositionRequest.getLevel());
        createPosition.setLimitDistance(createPositionRequest.getLimitDistance());
        createPosition.setSize(createPositionRequest.getSize());
        createPosition.setStopDistance(createPositionRequest.getStopDistance());
        createPosition.setTimeInForce(TimeInForce.valueOf(createPositionRequest.getTimeInForce().name()));
        createPosition.setType(Type.valueOf(createPositionRequest.getType().name()));
        CreateOTCWorkingOrderV2Response createPositionResponce
                = api.createOTCWorkingOrderV2(authenticationContext.getConversationContext(), createPosition);
        GetDealConfirmationV1Response dealConfirmationResponse = api.getDealConfirmationV1(authenticationContext.getConversationContext(), createPositionResponce.getDealReference());
        return new CreatePositionResponse(CreatePositionResponse.Status.valueOf(dealConfirmationResponse.getDealStatus().name()),
                dealConfirmationResponse.getReason().name());
    }

    @Override
    public List<OrderItem> getWorkingOrders() throws Exception {
        GetWorkingOrdersV2Response workingOrdersResponse = api.getWorkingOrdersV2(authenticationContext.getConversationContext());
        return igForexMarketDataConverter.convertOrderItems(workingOrdersResponse.getWorkingOrders());
    }

}
