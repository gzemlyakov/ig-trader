package com.gzemlyakov.ig.trader.core.iggroup.data.market;

import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface MarketObserver {

    void onUpdate(MarketItemUpdateInfo marketItemUpdateInfo);

}
