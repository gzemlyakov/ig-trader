package com.gzemlyakov.ig.trader.core.model.position;

import java.math.BigDecimal;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class CreatePositionRequest {

    private String currencyCode;
    private Direction direction;
    private String epic;
    private String expiry;
    private boolean forceOpen;
    private String goodTillDate;
    private boolean guaranteedStop;
    private BigDecimal level;
    private BigDecimal limitDistance;
    private BigDecimal size;
    private BigDecimal stopDistance;
    private TimeInForce timeInForce;
    private OrderType type;

    private CreatePositionRequest(String currencyCode, Direction direction, String epic, String expiry,
                                 boolean guaranteedStop, BigDecimal level, BigDecimal size, TimeInForce timeInForce,
                                 OrderType type) {
        this.currencyCode = currencyCode;
        this.direction = direction;
        this.epic = epic;
        this.expiry = expiry;
        this.guaranteedStop = guaranteedStop;
        this.level = level;
        this.size = size;
        this.timeInForce = timeInForce;
        this.type = type;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Direction getDirection() {
        return direction;
    }

    public String getEpic() {
        return epic;
    }

    public String getExpiry() {
        return expiry;
    }

    public boolean isForceOpen() {
        return forceOpen;
    }

    public String getGoodTillDate() {
        return goodTillDate;
    }

    public boolean isGuaranteedStop() {
        return guaranteedStop;
    }

    public BigDecimal getLevel() {
        return level;
    }

    public BigDecimal getLimitDistance() {
        return limitDistance;
    }

    public BigDecimal getSize() {
        return size;
    }

    public BigDecimal getStopDistance() {
        return stopDistance;
    }

    public TimeInForce getTimeInForce() {
        return timeInForce;
    }

    public OrderType getType() {
        return type;
    }

    public static CreatePositionRequestBuilder getBuilder() {
        return new CreatePositionRequestBuilder();
    }

    public static class CreatePositionRequestBuilder {

        private String currencyCode;
        private Direction direction;
        private String epic;
        private String expiry;
        private Boolean forceOpen;
        private String goodTillDate;
        private Boolean guaranteedStop;
        private BigDecimal level;
        private BigDecimal limitDistance;
        private BigDecimal size;
        private BigDecimal stopDistance;
        private TimeInForce timeInForce;
        private OrderType type;

        private CreatePositionRequestBuilder() {}

        public String getCurrencyCode() {
            return currencyCode;
        }

        public CreatePositionRequestBuilder setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Direction getDirection() {
            return direction;
        }

        public CreatePositionRequestBuilder setDirection(Direction direction) {
            this.direction = direction;
            return this;
        }

        public String getEpic() {
            return epic;
        }

        public CreatePositionRequestBuilder setEpic(String epic) {
            this.epic = epic;
            return this;
        }

        public String getExpiry() {
            return expiry;
        }

        public CreatePositionRequestBuilder setExpiry(String expiry) {
            this.expiry = expiry;
            return this;
        }

        public boolean isForceOpen() {
            return forceOpen;
        }

        public CreatePositionRequestBuilder setForceOpen(boolean forceOpen) {
            this.forceOpen = forceOpen;
            return this;
        }

        public String getGoodTillDate() {
            return goodTillDate;
        }

        public CreatePositionRequestBuilder setGoodTillDate(String goodTillDate) {
            this.goodTillDate = goodTillDate;
            return this;
        }

        public boolean isGuaranteedStop() {
            return guaranteedStop;
        }

        public CreatePositionRequestBuilder setGuaranteedStop(boolean guaranteedStop) {
            this.guaranteedStop = guaranteedStop;
            return this;
        }

        public BigDecimal getLevel() {
            return level;
        }

        public CreatePositionRequestBuilder setLevel(BigDecimal level) {
            this.level = level;
            return this;
        }

        public BigDecimal getLimitDistance() {
            return limitDistance;
        }

        public CreatePositionRequestBuilder setLimitDistance(BigDecimal limitDistance) {
            this.limitDistance = limitDistance;
            return this;
        }

        public BigDecimal getSize() {
            return size;
        }

        public CreatePositionRequestBuilder setSize(BigDecimal size) {
            this.size = size;
            return this;
        }

        public BigDecimal getStopDistance() {
            return stopDistance;
        }

        public CreatePositionRequestBuilder setStopDistance(BigDecimal stopDistance) {
            this.stopDistance = stopDistance;
            return this;
        }

        public TimeInForce getTimeInForce() {
            return timeInForce;
        }

        public CreatePositionRequestBuilder setTimeInForce(TimeInForce timeInForce) {
            this.timeInForce = timeInForce;
            return this;
        }

        public OrderType getType() {
            return type;
        }

        public CreatePositionRequestBuilder setType(OrderType type) {
            this.type = type;
            return this;
        }

        public CreatePositionRequest build() throws Exception {
            if (!isValid()) throw new Exception("Builder does not contains all required parameters");
            return new CreatePositionRequest(currencyCode, direction, epic, expiry, guaranteedStop, level, size, timeInForce, type);
        }

        private boolean isValid() {
            return currencyCode != null && direction != null && epic != null && expiry != null && guaranteedStop != null
                    && level != null && size != null && timeInForce != null && type != null;
        }

    }

    public enum Direction {
        BUY, SELL
    }

    public enum OrderType {
        LIMIT, STOP;
    }

    public enum TimeInForce {
        GOOD_TILL_CANCELLED, GOOD_TILL_DATE
    }

}
