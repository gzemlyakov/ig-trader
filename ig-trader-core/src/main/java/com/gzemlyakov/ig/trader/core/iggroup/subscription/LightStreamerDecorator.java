package com.gzemlyakov.ig.trader.core.iggroup.subscription;

import com.iggroup.api.streaming.HandyTableListenerAdapter;
import com.iggroup.api.streaming.LightStreamerComponent;
import com.lightstreamer.ls_client.SubscribedTableKey;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class LightStreamerDecorator extends LightStreamerComponent {

    private Set<SubscribedTableKey> subscriptions;

    public LightStreamerDecorator() {
        subscriptions = new HashSet<>();
    }

    public HandyTableListenerAdapter subscribeForMarket(String epic, HandyTableListenerAdapter adapter) throws Exception {
        HandyTableListenerAdapter subscriber = super.subscribeForMarket(epic, adapter);
        subscriptions.add(subscriber.getSubscribedTableKey());
        return subscriber;
    }

    public void unsubscribeAll() throws Exception {
        for (SubscribedTableKey subscription : subscriptions)
            super.unsubscribe(subscription);
    }

}
