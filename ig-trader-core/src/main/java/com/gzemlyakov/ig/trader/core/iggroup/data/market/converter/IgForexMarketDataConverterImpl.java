package com.gzemlyakov.ig.trader.core.iggroup.data.market.converter;

import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;
import com.gzemlyakov.ig.trader.core.model.market.ValueItem;
import com.gzemlyakov.ig.trader.core.model.order.OrderItem;
import com.gzemlyakov.ig.trader.core.model.position.WorkingOrder;
import com.iggroup.api.prices.getPricesByNumberOfPointsV2.PricesItem;
import com.iggroup.api.workingorders.getWorkingOrdersV2.GetWorkingOrdersV2Response;
import com.iggroup.api.workingorders.getWorkingOrdersV2.WorkingOrdersItem;
import com.lightstreamer.ls_client.UpdateInfo;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class IgForexMarketDataConverterImpl implements IgForexMarketDataConverter {

    private static final Pattern MARKET_PATTERN = Pattern.compile("MARKET:(.*?)$");

    private static final double DEFAULT_PRICE_VALUE = -1d;

    private final Logger logger = LoggerFactory.getLogger(IgForexMarketDataConverterImpl.class);

    private DateTimeFormatter createdDateFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd kk:mm:ss:SSS");
    private DateTimeFormatter goodTillDateFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd kk:mm:ss:SSS");

    @Override
    public MarketItemUpdateInfo convertItemUpdateInfo(UpdateInfo updateInfo) throws Exception {
        Matcher matcher = MARKET_PATTERN.matcher(updateInfo.getItemName());
        if (!matcher.find()) {
            logger.warn("Extract epic name from market item name failed");
            throw new Exception("Conversion failed. Unable to extract market item name");
        }
        String epic = matcher.group(1);
        ValueItem sell = new ValueItem(stringValToDouble(updateInfo.getOldValue(1)),
                stringValToDouble(updateInfo.getNewValue(1)));
        ValueItem buy = new ValueItem(stringValToDouble(updateInfo.getOldValue(2)),
                stringValToDouble(updateInfo.getNewValue(2)));
        return new MarketItemUpdateInfo(epic, updateInfo.isValueChanged(1), updateInfo.isValueChanged(2), sell, buy);
    }

    @Override
    public List<PriceItem> convertPriceItems(List<PricesItem> source) {
        List<PriceItem> result = new ArrayList<>();
        for (PricesItem item : source) result.add(convertPriceItem(item));
        return result;
    }

    @Override
    public PriceItem convertPriceItem(PricesItem source) {
        PriceItem target = new PriceItem();
        if (source.getSnapshotTime() != null) target.setSnapshotTime(source.getSnapshotTime());
        if (source.getOpenPrice() != null) target.setOpenPrice(source.getOpenPrice().getBid());
        if (source.getClosePrice() != null) target.setClosePrice(source.getClosePrice().getBid());
        if (source.getHighPrice() != null) target.setHighPrice(source.getHighPrice().getBid());
        if (source.getLowPrice() != null) target.setLowPrice(source.getLowPrice().getBid());
        return target;
    }

    @Override
    public List<MarketItem> convertMarketItems() {
        return null;
    }

    @Override
    public List<WorkingOrder> convertOrderItems(GetWorkingOrdersV2Response workingOrdersResponse) {
        List<WorkingOrder> result = new ArrayList<>();
        for (WorkingOrdersItem workingOrdersItem : workingOrdersResponse.getWorkingOrders()) {
            WorkingOrder workingOrder = new WorkingOrder();
            workingOrder.setInstrumentName(workingOrdersItem.getMarketData().getInstrumentName());
//            workingOrder.setCreatedDate(); TODO Uncomment
            workingOrder.setCurrencyCode(workingOrdersItem.getWorkingOrderData().getCurrencyCode());
            workingOrder.setDirection(workingOrdersItem.getWorkingOrderData().getDirection().name());
//            workingOrder.setGoodTillDate(); TODO Uncomment
            workingOrder.setLimitDistance(workingOrdersItem.getWorkingOrderData().getLimitDistance());
            workingOrder.setOrderLevel(workingOrdersItem.getWorkingOrderData().getOrderLevel());
            workingOrder.setOrderSize(workingOrdersItem.getWorkingOrderData().getOrderSize());
            workingOrder.setOrderType(workingOrdersItem.getWorkingOrderData().getOrderType().name());
            workingOrder.setStopDistance(workingOrdersItem.getWorkingOrderData().getStopDistance());
            result.add(workingOrder);
        }
        return result;
    }

    @Override
    public List<OrderItem> convertOrderItems(List<WorkingOrdersItem> workingOrdersItems) {
        List<OrderItem> result = new ArrayList<>();
        for (WorkingOrdersItem workingOrdersItem : workingOrdersItems) {
            OrderItem orderItem = new OrderItem();
            orderItem.setSymbol(workingOrdersItem.getMarketData().getInstrumentName());
            orderItem.setBid(new BigDecimal(workingOrdersItem.getMarketData().getBid()));
            orderItem.setCreatedDate(workingOrdersItem.getWorkingOrderData().getCreatedDate());
            orderItem.setDirection(workingOrdersItem.getWorkingOrderData().getDirection().name());
            orderItem.setTimeInForce(workingOrdersItem.getWorkingOrderData().getTimeInForce().name());
            orderItem.setGoodTillDate(workingOrdersItem.getWorkingOrderData().getGoodTillDate());
            orderItem.setGuaranteedStop(workingOrdersItem.getWorkingOrderData().getGuaranteedStop());
            orderItem.setLimitDistance(workingOrdersItem.getWorkingOrderData().getLimitDistance());
            orderItem.setStopDistance(workingOrdersItem.getWorkingOrderData().getStopDistance());
            orderItem.setOrderLevel(workingOrdersItem.getWorkingOrderData().getOrderLevel());
            orderItem.setSize(workingOrdersItem.getMarketData().getLotSize());
            orderItem.setOrderType(workingOrdersItem.getWorkingOrderData().getOrderType().name());
            result.add(orderItem);
        }
        return result;
    }

    private double stringValToDouble(String val) {
        return NumberUtils.toDouble(val, DEFAULT_PRICE_VALUE);
    }

}
