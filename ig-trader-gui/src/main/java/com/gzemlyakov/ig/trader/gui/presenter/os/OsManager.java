package com.gzemlyakov.ig.trader.gui.presenter.os;

import java.io.IOException;
import java.net.URI;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface OsManager {

    void browsePage(URI uri) throws IOException;

}
