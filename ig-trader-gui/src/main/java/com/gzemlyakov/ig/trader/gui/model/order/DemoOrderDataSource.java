package com.gzemlyakov.ig.trader.gui.model.order;

import com.gzemlyakov.ig.trader.gui.model.market.ForexMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class DemoOrderDataSource {

    @Autowired
    private ForexMarket forexMarket;

    private ObservableList<OrderItem> openOrders;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public DemoOrderDataSource() {
        this.openOrders = FXCollections.observableArrayList();
    }

    public void createOrderItem(OrderForm orderForm) {
        OrderItem orderItem = new OrderItem();
        orderItem.setSymbol(forexMarket.getMarketItemByEpic(orderForm.getEpic()).getSymbol());
        orderItem.setCreatedDate(LocalDateTime.now().format(formatter));
        orderItem.setDirection(orderForm.getDirection());
        orderItem.setTimeInForce(orderForm.getTimeInForce());
        orderItem.setGoodTillDate(orderForm.getGoodTillDate());
        orderItem.setGuaranteedStop(orderForm.getGuaranteedStop());
        orderItem.setLimitDistance(orderForm.getLimitDistance());
        orderItem.setLimitDistance(orderForm.getLimitDistance());
        orderItem.setStopDistance(orderForm.getStopDistance());
        orderItem.setOrderLevel(orderForm.getOrderLevel());
        orderItem.setSize(orderForm.getSize());
        orderItem.setOrderType(orderForm.getType());
        openOrders.add(orderItem);
    }

    public ObservableList<OrderItem> getOrderItems() {
        return openOrders;
    }

}
