package com.gzemlyakov.ig.trader.gui.model.chart;

import javafx.beans.NamedArg;
import javafx.util.Pair;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class ComboBoxPair extends Pair<String, String> {
    /**
     * Creates a new pair
     *
     * @param key   The key for this pair
     * @param value The value to use for this pair
     */
    public ComboBoxPair(@NamedArg("key") String key, @NamedArg("value") String value) {
        super(key, value);
    }

    @Override
    public String toString() {
        return getValue();
    }

}
