package com.gzemlyakov.ig.trader.gui.model.order;

import javafx.beans.property.*;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class OrderItem {

    private StringProperty symbol = new SimpleStringProperty(this, "symbol");
//    private DoubleProperty bid = new SimpleDoubleProperty(this, "bid");
    private StringProperty createdDate = new SimpleStringProperty(this, "createdDate");
    private StringProperty direction = new SimpleStringProperty(this, "direction");
    private StringProperty timeInForce = new SimpleStringProperty(this, "timeInForce");
    private StringProperty goodTillDate = new SimpleStringProperty(this, "goodTillDate");
    private BooleanProperty guaranteedStop = new SimpleBooleanProperty(this, "guaranteedStop");
    private DoubleProperty limitDistance = new SimpleDoubleProperty(this, "limitDistance");
    private DoubleProperty stopDistance = new SimpleDoubleProperty(this, "stopDistance");
    private DoubleProperty orderLevel = new SimpleDoubleProperty(this, "orderLevel");
    private DoubleProperty size = new SimpleDoubleProperty(this, "size");
    private StringProperty orderType = new SimpleStringProperty(this, "orderType");

    public String getSymbol() {
        return symbol.get();
    }

    public StringProperty symbolProperty() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol.set(symbol);
    }

/*
    public double getBid() {
        return bid.get();
    }

    public DoubleProperty bidProperty() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid.set(bid);
    }
*/

    public String getCreatedDate() {
        return createdDate.get();
    }

    public StringProperty createdDateProperty() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate.set(createdDate);
    }

    public String getDirection() {
        return direction.get();
    }

    public StringProperty directionProperty() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction.set(direction);
    }

    public String getTimeInForce() {
        return timeInForce.get();
    }

    public StringProperty timeInForceProperty() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce.set(timeInForce);
    }

    public String getGoodTillDate() {
        return goodTillDate.get();
    }

    public StringProperty goodTillDateProperty() {
        return goodTillDate;
    }

    public void setGoodTillDate(String goodTillDate) {
        this.goodTillDate.set(goodTillDate);
    }

    public boolean getGuaranteedStop() {
        return guaranteedStop.get();
    }

    public BooleanProperty guaranteedStopProperty() {
        return guaranteedStop;
    }

    public void setGuaranteedStop(boolean guaranteedStop) {
        this.guaranteedStop.set(guaranteedStop);
    }

    public double getLimitDistance() {
        return limitDistance.get();
    }

    public DoubleProperty limitDistanceProperty() {
        return limitDistance;
    }

    public void setLimitDistance(double limitDistance) {
        this.limitDistance.set(limitDistance);
    }

    public double getStopDistance() {
        return stopDistance.get();
    }

    public DoubleProperty stopDistanceProperty() {
        return stopDistance;
    }

    public void setStopDistance(double stopDistance) {
        this.stopDistance.set(stopDistance);
    }

    public double getOrderLevel() {
        return orderLevel.get();
    }

    public DoubleProperty orderLevelProperty() {
        return orderLevel;
    }

    public void setOrderLevel(double orderLevel) {
        this.orderLevel.set(orderLevel);
    }

    public double getSize() {
        return size.get();
    }

    public DoubleProperty sizeProperty() {
        return size;
    }

    public void setSize(double size) {
        this.size.set(size);
    }

    public String getOrderType() {
        return orderType.get();
    }

    public StringProperty orderTypeProperty() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType.set(orderType);
    }

}
