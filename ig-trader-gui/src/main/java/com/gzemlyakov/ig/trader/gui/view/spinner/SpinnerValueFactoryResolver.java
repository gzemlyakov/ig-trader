package com.gzemlyakov.ig.trader.gui.view.spinner;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface SpinnerValueFactoryResolver<S, T> {

    SpinnerValueFactory<T> getSpinnerValueFactory(Spinner<T> spinner, S bounder);

}
