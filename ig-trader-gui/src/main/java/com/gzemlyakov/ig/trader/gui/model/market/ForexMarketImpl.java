package com.gzemlyakov.ig.trader.gui.model.market;

import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketObserver;
import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;
import com.gzemlyakov.ig.trader.core.model.market.ValueItem;
import com.gzemlyakov.ig.trader.gui.model.order.OrderItem;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ForexMarketImpl implements ForexMarket, MarketObserver {

    private Map<String, MarketItem> marketItemMap;
    private ObservableList<MarketItem> marketItems;
    private ObservableList<OrderItem> orderItems = FXCollections.observableArrayList();

/*
    @Override
    public void init(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> symbols) {
        marketItemMap = new HashMap<>();
        for (com.gzemlyakov.ig.trader.core.model.market.MarketItem source : symbols) {
            MarketItem marketItem = new MarketItem();
            marketItem.setEpic(source.getEpic());
            marketItem.setExpiry(source.getExpiry());
            marketItem.setSymbol(source.getSymbol());
            marketItem.setCurrencyCodes(source.getCurrencies());
            marketItem.setMinNormalStopOrLimitDistance(source.getMinNormalStopOrLimitDistance());
            marketItem.setScalingFactor(source.getScalingFactor());
            marketItem.setMinControlledRiskStopDistance(source.getMinControlledRiskStopDistance());
            marketItemMap.put(source.getEpic(), marketItem);
        }
    }
*/

    @Override
    public void init(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> symbols,
                      List<com.gzemlyakov.ig.trader.core.model.order.OrderItem> openOrders) {
        initSymbols(symbols);
        initOpenOrders(openOrders);
    }

    @Override
    public void initSymbols(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> symbols) {
        marketItemMap = new HashMap<>();
        for (com.gzemlyakov.ig.trader.core.model.market.MarketItem source : symbols) {
            MarketItem marketItem = new MarketItem();
            marketItem.setEpic(source.getEpic());
            marketItem.setExpiry(source.getExpiry());
            marketItem.setSymbol(source.getSymbol());
            marketItem.setCurrencyCodes(source.getCurrencies());
            marketItem.setMinNormalStopOrLimitDistance(source.getMinNormalStopOrLimitDistance());
            marketItem.setScalingFactor(source.getScalingFactor());
            marketItem.setMinControlledRiskStopDistance(source.getMinControlledRiskStopDistance());
            marketItemMap.put(source.getEpic(), marketItem);
        }
    }

    @Override
    public void initOpenOrders(List<com.gzemlyakov.ig.trader.core.model.order.OrderItem> openOrders) {
        orderItems.clear();
        for (com.gzemlyakov.ig.trader.core.model.order.OrderItem source : openOrders) {
            OrderItem orderItem = new OrderItem();
            orderItem.setSymbol(source.getSymbol());
//            if (source.getBid() != null) orderItem.setBid(source.getBid().doubleValue());
            orderItem.setCreatedDate(source.getCreatedDate());
            orderItem.setDirection(source.getDirection());
            orderItem.setTimeInForce(source.getTimeInForce());
            orderItem.setGoodTillDate(source.getGoodTillDate());
            orderItem.setGuaranteedStop(source.getGuaranteedStop());
            if (source.getLimitDistance() != null) orderItem.setLimitDistance(source.getLimitDistance().doubleValue());
            if (source.getStopDistance() != null) orderItem.setStopDistance(source.getStopDistance().doubleValue());
            if (source.getOrderLevel() != null) orderItem.setOrderLevel(source.getOrderLevel().doubleValue());
            orderItem.setSize(source.getSize());
            orderItem.setOrderType(source.getOrderType());
            orderItems.add(orderItem);
        }
    }

    @Override
    public void onUpdate(MarketItemUpdateInfo marketItemUpdateInfo) {
        MarketItem itemToUpdate = marketItemMap.get(marketItemUpdateInfo.getEpic());
        itemToUpdate.setBuyValue(marketItemUpdateInfo.getBuyValue());
        itemToUpdate.setSellValue(marketItemUpdateInfo.getSellValue());
    }

    @Override
    public ObservableList<MarketItem> getMarketItems() {
        if (marketItemMap == null)
            throw new IllegalStateException("Forex market should be initialized. Use #init method");
        if (marketItems != null) return marketItems;
        return (marketItems = FXCollections.observableArrayList(marketItemMap.values()));
    }

    @Override
    public MarketItem getMarketItemByEpic(String epic) {
        return marketItemMap.containsKey(epic) ? marketItemMap.get(epic) : null;
    }

    @Override
    public ObservableList<OrderItem> getOrderItems() {
        return orderItems;
    }

}
