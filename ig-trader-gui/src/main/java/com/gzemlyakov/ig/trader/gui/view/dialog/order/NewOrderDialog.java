package com.gzemlyakov.ig.trader.gui.view.dialog.order;

import com.gzemlyakov.ig.trader.gui.view.dialog.AbstractDialog;
import com.gzemlyakov.ig.trader.gui.view.dialog.CustomDialog;
import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class NewOrderDialog extends AbstractDialog<Pair<String, String>> {

    @Autowired
    public NewOrderDialog(ResourceManager resourceManager, FXMLLoader fxmlLoader) {
        super(resourceManager, fxmlLoader);
    }

    @Override
    protected Double getDialogWidth() {
        return null;
    }

    @Override
    protected Double getDialogHeight() {
        return null;
    }

    @Override
    protected String getFxmlName() {
        return "dialog/order/new-order-dialog.fxml";
    }

    @Override
    protected String getDialogTitle() {
        return resourceManager.getLocalizedString("dialog.new.order");
    }

    @Override
    protected String getDialogHeaderText() {
        return null;
    }

    @Override
    protected String getIconName() {
        return "new-order.png";
    }

}
