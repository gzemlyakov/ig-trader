package com.gzemlyakov.ig.trader.gui.view.builder;

import com.gzemlyakov.ig.trader.gui.view.builder.chart.CandleStickChartBuilder;
import com.soilprod.javafx.chart.candlestick.CandleStickChart;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.util.Builder;
import javafx.util.BuilderFactory;
import org.springframework.stereotype.Component;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class BuilderFactoryImpl implements BuilderFactory {

    private final JavaFXBuilderFactory fxBuilderFactory = new JavaFXBuilderFactory();

    @Override
    public Builder<?> getBuilder(Class<?> type) {
        if (type.equals(CandleStickChart.class)) {
            return new CandleStickChartBuilder();
        }
        return fxBuilderFactory.getBuilder(type);
    }

}
