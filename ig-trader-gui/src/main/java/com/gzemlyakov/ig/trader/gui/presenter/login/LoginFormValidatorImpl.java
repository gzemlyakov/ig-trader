package com.gzemlyakov.ig.trader.gui.presenter.login;

import com.gzemlyakov.ig.trader.gui.model.credentials.LoginFormModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class LoginFormValidatorImpl implements LoginFormValidator {

    @Override
    public boolean isLoginValid(LoginFormModel loginModel) {
        return StringUtils.isNotEmpty(loginModel.getLogIn());
    }

    @Override
    public boolean isPasswordValid(LoginFormModel loginModel) {
        return StringUtils.isNotEmpty(loginModel.getPassword());
    }

    @Override
    public boolean isApiKeyValid(LoginFormModel loginModel) {
        return StringUtils.isNotEmpty(loginModel.getApiKey());
    }

}
