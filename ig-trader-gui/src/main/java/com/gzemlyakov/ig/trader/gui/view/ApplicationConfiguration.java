package com.gzemlyakov.ig.trader.gui.view;

import com.gzemlyakov.ig.trader.gui.view.builder.BuilderFactoryImpl;
import javafx.fxml.FXMLLoader;
import javafx.util.BuilderFactory;
import org.springframework.context.annotation.*;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Configuration
@ComponentScan(basePackages = "com.gzemlyakov.ig.trader.gui")
@Import(com.gzemlyakov.ig.trader.core.ApplicationConfiguration.class)
public class ApplicationConfiguration {//TODO add logging and remove extra css

    @Bean
    BuilderFactory builderFactory() {
        return new BuilderFactoryImpl();
    }

    @Bean
    @Scope("prototype")
    FXMLLoader fxmlLoader() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(ResourceBundle.getBundle("i18n.i18n", Locale.getDefault()));
        fxmlLoader.setBuilderFactory(builderFactory());
        return fxmlLoader;
    }

}
