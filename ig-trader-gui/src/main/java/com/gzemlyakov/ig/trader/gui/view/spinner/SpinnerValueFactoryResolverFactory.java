package com.gzemlyakov.ig.trader.gui.view.spinner;

import com.gzemlyakov.ig.trader.gui.model.market.MarketItem;
import com.gzemlyakov.ig.trader.gui.presenter.util.MathUtils;
import javafx.scene.control.SpinnerValueFactory;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class SpinnerValueFactoryResolverFactory {

    public static SpinnerValueFactoryResolver<MarketItem, Double> orderLevelValueFactory() {
        return (spinner, bounder) -> {
            if (bounder.getSellValue() == null) return spinner.getValueFactory();
            return new SpinnerValueFactory.DoubleSpinnerValueFactory(Double.MIN_VALUE, Double.MAX_VALUE,
                    bounder.getSellValue().getNewValue(), MathUtils.getSpinnerStep(bounder.getSellValue().getNewValue()));
        };
    }

    public static SpinnerValueFactoryResolver<MarketItem, Double> stopOrderValueFactory() {
        return (spinner, bounder) -> {
            if (bounder.getSellValue() == null) return spinner.getValueFactory();
            return new SpinnerValueFactory.DoubleSpinnerValueFactory(Double.MIN_VALUE, Double.MAX_VALUE,
                    bounder.getSellValue().getNewValue(), MathUtils.getSpinnerStep(bounder.getSellValue().getNewValue()));
        };
    }

}
