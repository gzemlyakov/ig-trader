package com.gzemlyakov.ig.trader.gui.view.resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.gzemlyakov.ig.trader.gui.view.constant.ErrorMessage.*;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ResourceManagerImpl implements ResourceManager{

    private ClassLoader classLoader;
    private ResourceBundle localizedResources;

    public ResourceManagerImpl() {
        this.classLoader = getClass().getClassLoader();
        localizedResources = ResourceBundle.getBundle("i18n.i18n", Locale.getDefault());
    }

    @Override
    public String getIcon(String icon) throws IOException {
        return getResource(ResourceFolder.ICONS, icon);
    }

    @Override
    public String getStyleSheet(String name) throws IOException {
        return getResource(ResourceFolder.CSS, name);
    }

    @Override
    public URL getFxml(String name) throws IOException {
        return getUrlResource(ResourceFolder.FXML, name);
    }

    @Override
    public String getLocalizedString(String key) {
        return localizedResources.getString(key);
    }

    private String getResource(ResourceFolder folder, String name) throws IOException  {
        return getUrlResource(folder, name).toString();
    }

    private URL getUrlResource(ResourceFolder folder, String name) throws IOException  {
        if (StringUtils.isEmpty(name))
            throw new IOException(String.format(WRONG_RESOURCE_NAME.toString(), name));
        URL resourceUrl = classLoader.getResource(folder.path + name);
        if (resourceUrl == null)
            throw new IOException(String.format(WRONG_RESOURCE_NAME.toString(), name));
        return resourceUrl;
    }

    private enum ResourceFolder {

        ICONS("icons/"),
        CSS("css/"),
        FXML("fxml/");

        String path;

        ResourceFolder(String path) {
            this.path = path;
        }

    }
}
