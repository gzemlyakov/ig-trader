package com.gzemlyakov.ig.trader.gui.view.label;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface LabelFormatter<T> {

    String format(T obj);

}
