package com.gzemlyakov.ig.trader.gui.view.constant;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public enum ErrorMessage {

    WRONG_RESOURCE_NAME("Unable to find resource %s"),

    GUI_INIT_BEFORE_SHOW("Unable to show GUI. Initialize GUI using init method before show.");

    private String errorMessage;

    ErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return errorMessage;
    }

}
