package com.gzemlyakov.ig.trader.gui.view.composite;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface Composite {

    <T extends Pane> T getMainPane();

    <T extends Node> T lookupChild(String selector);

}
