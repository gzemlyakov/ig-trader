package com.gzemlyakov.ig.trader.gui.presenter;

import com.gzemlyakov.ig.trader.gui.view.ForexTraderGui;
import javafx.stage.Stage;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface ForexTraderPresenter {

    void init(Stage primaryStage);

    void launch();

    ForexTraderGui getView();

}
