package com.gzemlyakov.ig.trader.gui.model.market;

import com.gzemlyakov.ig.trader.core.model.market.ValueItem;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.List;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class MarketItem {

    private StringProperty symbol = new SimpleStringProperty(this, "symbol");
    private StringProperty epic = new SimpleStringProperty(this, "epic");
    private StringProperty expiry = new SimpleStringProperty(this, "expiry");
    private ObjectProperty<ValueItem> buyValue = new SimpleObjectProperty<>(this, "buy");
    private ObjectProperty<ValueItem> sellValue = new SimpleObjectProperty<>(this, "sell");
    private ObjectProperty<List<String>> currencyCodes = new SimpleObjectProperty<>(this, "currencyCodes");
    private SimpleDoubleProperty minNormalStopOrLimitDistance = new SimpleDoubleProperty(this, "minNormalStopOrLimitDistance");
    private IntegerProperty scalingFactor = new SimpleIntegerProperty(this, "scalingFactor");
    private DoubleProperty minControlledRiskStopDistance = new SimpleDoubleProperty(this, "minControlledRiskStopDistance");

    public String getSymbol() {
        return symbol.get();
    }

    public StringProperty symbolProperty() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol.set(symbol);
    }

    public ValueItem getBuyValue() {
        return buyValue.get();
    }

    public ObjectProperty<ValueItem> buyValueProperty() {
        return buyValue;
    }

    public void setBuyValue(ValueItem buyValue) {
        this.buyValue.set(buyValue);
    }

    public ValueItem getSellValue() {
        return sellValue.get();
    }

    public ObjectProperty<ValueItem> sellValueProperty() {
        return sellValue;
    }

    public void setSellValue(ValueItem sellValue) {
        this.sellValue.set(sellValue);
    }

    public String getEpic() {
        return epic.get();
    }

    public StringProperty epicProperty() {
        return epic;
    }

    public void setEpic(String epic) {
        this.epic.set(epic);
    }

    public String getExpiry() {
        return expiry.get();
    }

    public StringProperty expiryProperty() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry.set(expiry);
    }

    public List<String> getCurrencyCodes() {
        return currencyCodes.get();
    }

    public ObjectProperty<List<String>> currencyCodesProperty() {
        return currencyCodes;
    }

    public void setCurrencyCodes(List<String> currencyCodes) {
        this.currencyCodes.set(currencyCodes);
    }

    public Double getMinNormalStopOrLimitDistance() {
        return minNormalStopOrLimitDistance.get();
    }

    public SimpleDoubleProperty minNormalStopOrLimitDistanceProperty() {
        return minNormalStopOrLimitDistance;
    }

    public void setMinNormalStopOrLimitDistance(Double minNormalStopOrLimitDistance) {
        this.minNormalStopOrLimitDistance.set(minNormalStopOrLimitDistance);
    }

    public int getScalingFactor() {
        return scalingFactor.get();
    }

    public IntegerProperty scalingFactorProperty() {
        return scalingFactor;
    }

    public void setScalingFactor(int scalingFactor) {
        this.scalingFactor.set(scalingFactor);
    }

    public double getMinControlledRiskStopDistance() {
        return minControlledRiskStopDistance.get();
    }

    public DoubleProperty minControlledRiskStopDistanceProperty() {
        return minControlledRiskStopDistance;
    }

    public void setMinControlledRiskStopDistance(double minControlledRiskStopDistance) {
        this.minControlledRiskStopDistance.set(minControlledRiskStopDistance);
    }

    @Override
    public String toString() {
        return symbol.get();
    }

}
