package com.gzemlyakov.ig.trader.gui.view.dialog;

import com.gzemlyakov.ig.trader.gui.view.composite.Composite;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface CustomDialog extends Composite {

    void show();

    void hide();

}
