package com.gzemlyakov.ig.trader.gui.presenter.os;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class OsManagerImpl implements OsManager {

    @Override
    public void browsePage(URI uri) throws IOException {
        if(Desktop.isDesktopSupported()) {
            Desktop.getDesktop().browse(uri);
        }
    }

}
