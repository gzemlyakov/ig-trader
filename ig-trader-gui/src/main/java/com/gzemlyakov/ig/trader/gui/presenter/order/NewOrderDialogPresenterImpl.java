package com.gzemlyakov.ig.trader.gui.presenter.order;

import com.gzemlyakov.ig.trader.core.TraderFacade;
import com.gzemlyakov.ig.trader.core.model.market.ValueItem;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionRequest;
import com.gzemlyakov.ig.trader.core.model.position.CreatePositionResponse;
import com.gzemlyakov.ig.trader.gui.model.market.ForexMarket;
import com.gzemlyakov.ig.trader.gui.model.market.MarketItem;
import com.gzemlyakov.ig.trader.gui.model.order.DemoOrderDataSource;
import com.gzemlyakov.ig.trader.gui.model.order.OrderForm;
import com.gzemlyakov.ig.trader.gui.presenter.order.error.ErrorMessageResolver;
import com.gzemlyakov.ig.trader.gui.presenter.util.DateUtils;
import com.gzemlyakov.ig.trader.gui.presenter.util.MathUtils;
import com.gzemlyakov.ig.trader.gui.view.dialog.CustomDialog;
import com.gzemlyakov.ig.trader.gui.view.dialog.order.NewOrderDialog;
import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import jfxtras.scene.control.CalendarTextField;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class NewOrderDialogPresenterImpl implements NewOrderDialogPresenter {

    private static final Tooltip DATE_ERROR_TOOLTIP = new Tooltip("Specify date in future");

    private final Logger logger = LoggerFactory.getLogger(NewOrderDialogPresenterImpl.class);

    private static final String BUY = "BUY";
    private static final String SELL = "SELL";

    private CustomDialog newOrderDialog;
    private TraderFacade traderFacade;
    private boolean isDemo;//TODO Use user preferences instead

    @Autowired
    private ForexMarket forexMarket;

    @Autowired
    private ResourceManager resourceManager;

    @Autowired
    private DemoOrderDataSource demoOrderDataSource;

    @Autowired
    private ErrorMessageResolver errorMessageResolver;

    @Autowired
    public NewOrderDialogPresenterImpl(NewOrderDialog newOrderDialog, TraderFacade traderFacade) {
        this.newOrderDialog = newOrderDialog;
        this.traderFacade = traderFacade;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init() {
        OrderForm orderForm = new OrderForm();
        ObservableList<MarketItem> marketItems = forexMarket.getMarketItems();
        if (CollectionUtils.isEmpty(marketItems)) throw new RuntimeException("Forex market is not initialized");
        ComboBox<MarketItem> symbolChooser = newOrderDialog.lookupChild("#new-order-symbol-chooser");

        Label currentLevel = newOrderDialog.lookupChild("#new-order-current-level-lbl");
        ComboBox<String> currencyCode = newOrderDialog.lookupChild("#new-order-currency-code-chooser");
        orderForm.currencyCodeProperty().bind(currencyCode.valueProperty());

        Spinner<Double> orderLevel = newOrderDialog.lookupChild("#new-order-order-level");
        orderLevel.getValueFactory().setConverter(new StringConverter<Double>() {
            @Override
            public String toString(Double object) {
                return object.toString();
            }

            @Override
            public Double fromString(String string) {
                return Double.valueOf(string);
            }
        });
        orderForm.orderLevelProperty().bind(orderLevel.valueProperty());

        TitledPane closeConditions = newOrderDialog.lookupChild("#close-conditions-panel");
        Spinner<Double> stopOrder = (Spinner<Double>) closeConditions.getContent().lookup("#new-order-stop");
        orderForm.stopDistanceProperty().bind(stopOrder.valueProperty());
        Label stopOrderLabel = (Label) closeConditions.getContent().lookup("#new-order-stop-label");
        CheckBox guaranteedStop = (CheckBox) closeConditions.getContent().lookup("#new-order-guaranteed-stop-checkbox");
        orderForm.guaranteedStopProperty().bind(guaranteedStop.selectedProperty());
        Spinner<Integer> limit = (Spinner<Integer>) closeConditions.getContent().lookup("#new-order-limit-spinner");
        orderForm.limitDistanceProperty().bind(limit.valueProperty());

        symbolChooser.valueProperty().addListener(getSymbolChangeListener(orderForm, currentLevel, currencyCode, orderLevel, stopOrder, guaranteedStop, stopOrderLabel));
        symbolChooser.setItems(marketItems);
        symbolChooser.setValue(marketItems.get(0));

        CheckBox forceOpen = newOrderDialog.lookupChild("#new-order-force-open-checkbox");
        orderForm.forceOpenProperty().bind(forceOpen.selectedProperty());

        ComboBox<String> type = newOrderDialog.lookupChild("#new-order-type");
        type.setItems(FXCollections.observableArrayList(
                resourceManager.getLocalizedString("dialog.new.order.combo.box.type.stop"),
                resourceManager.getLocalizedString("dialog.new.order.combo.box.type.limit")));
        orderForm.typeProperty().bind(type.valueProperty());
        type.setValue(type.getItems().get(0));

        TitledPane timeInForce = newOrderDialog.lookupChild("#time-in-force-panel");
        CalendarTextField dateTimePicker = (CalendarTextField) timeInForce.getContent().lookup("#new-order-date-time-picker");
        RadioButton dateTimeRb = (RadioButton) timeInForce.getContent().lookup("#new-order-date-time-rb");
        RadioButton cancelledRb = (RadioButton) timeInForce.getContent().lookup("#new-order-cancelled-rb");
        dateTimeRb.setOnAction(event -> dateTimePicker.setPickerShowing(true));
        cancelledRb.selectedProperty().addListener((observable, oldValue, newValue) -> {
            dateTimePicker.setDisable(newValue);
            dateTimePicker.getStyleClass().remove("error");
        });
        dateTimePicker.setValueValidationCallback(param -> {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            boolean isValid = param.compareTo(calendar) > 0;
            if (!isValid) {
                dateTimePicker.getStyleClass().add("error");
                dateTimePicker.setTooltip(DATE_ERROR_TOOLTIP);
            } else {
                dateTimePicker.getStyleClass().remove("error");
                dateTimePicker.setTooltip(null);
            }
            return isValid;
        });
        dateTimePicker.calendarProperty().addListener((observable, oldValue, newValue) -> {
            orderForm.setGoodTillDate(DateUtils.toUtcString(newValue));
            dateTimeRb.setSelected(true);
        });
        cancelledRb.setSelected(true);

        Spinner<Double> size = newOrderDialog.lookupChild("#new-order-size-spinner");
        orderForm.sizeProperty().bind(size.valueProperty());

        Button sell = newOrderDialog.lookupChild("#sell-button");
        sell.setOnAction(event -> processButtonClick(orderForm, SELL));
        Button buy = newOrderDialog.lookupChild("#buy-button");
        buy.setOnAction(event -> processButtonClick(orderForm, BUY));
    }

    private void processButtonClick(OrderForm orderForm, String buttonType) {
        if (!isValid(orderForm)) return;
        if (isDemo) {
            demoOrderDataSource.createOrderItem(orderForm);
            return;
        }
        CreatePositionResponse response = sendCreateOrderRequest(orderForm, buttonType);
        if (response.getStatus().equals(CreatePositionResponse.Status.REJECTED))
            showErrorAlert(errorMessageResolver.resolve(response.getReason()));
        else newOrderDialog.hide();
    }

    private boolean isValid(OrderForm orderForm) {
        ComboBox<String> currencyCode = newOrderDialog.lookupChild("#new-order-currency-code-chooser");
        if (StringUtils.isEmpty(orderForm.getCurrencyCode())) {
            currencyCode.getStyleClass().add("error");
            return false;
        } else {
            currencyCode.getStyleClass().remove("error");
        }
        Spinner<Double> orderLevel = newOrderDialog.lookupChild("#new-order-order-level");
        if (orderForm.getOrderLevel() == null) {
            orderLevel.getStyleClass().add("error");
            return false;
        } else {
            orderLevel.getStyleClass().remove("error");
        }
        Spinner<Double> size = newOrderDialog.lookupChild("#new-order-size-spinner");
        if (orderForm.getSize() == null) {
            size.getStyleClass().add("error");
            return false;
        } else {
            size.getStyleClass().remove("error");
        }
        return true;
    }

    @Override
    public void showNewOrderDialog() {
        newOrderDialog.show();
    }

    private ChangeListener<MarketItem> getSymbolChangeListener(final OrderForm orderForm, final Label currentLevel,
                                                               final ComboBox<String> currencyCode, final Spinner<Double> orderLevel,
                                                               final Spinner<Double> stopOrder, final CheckBox guaranteedStop, final Label stopOrderLabel) {//TODO Remove params and lookup this elements from newOrderDialog
        return new ChangeListener<MarketItem>() {

            private static final double PERCENT = 100;

            private MarketItem marketItem;
            private String minPointsPattern = resourceManager.getLocalizedString("dialog.new.order.label.stop.min.points");
            private String minPercentPatten = resourceManager.getLocalizedString("dialog.new.order.label.stop.min.percent");

            private final ChangeListener<ValueItem> changeListener = new ChangeListener<ValueItem>() {
                @Override
                public void changed(ObservableValue<? extends ValueItem> observable, ValueItem oldValue,
                                    ValueItem newValue) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            currentLevel.setText(String.format("%s / %s", marketItem.getBuyValue(), marketItem.getSellValue()));
                        }
                    });
                }
            };

            @Override
            public void changed(ObservableValue<? extends MarketItem> observable, MarketItem oldMarketItem,
                    MarketItem newMarketItem) {
                if (oldMarketItem != null) {
                    oldMarketItem.sellValueProperty().removeListener(changeListener);
                    oldMarketItem.buyValueProperty().removeListener(changeListener);
                }
                newMarketItem.sellValueProperty().addListener(changeListener);
                newMarketItem.buyValueProperty().addListener(changeListener);

                currencyCode.getItems().clear();
                currencyCode.setItems(FXCollections.observableList(newMarketItem.getCurrencyCodes()));
                currencyCode.setValue(currencyCode.getItems().get(0));

                if (newMarketItem.getBuyValue() != null) orderLevel.getValueFactory().setValue(newMarketItem.getBuyValue().getNewValue());
                else newMarketItem.buyValueProperty().addListener(new ChangeListener<ValueItem>() {
                    @Override
                    public void changed(ObservableValue<? extends ValueItem> observable, ValueItem oldValueItem, ValueItem newValueItem) {
                        orderLevel.getValueFactory().setValue(newValueItem.getNewValue());
                        ((SpinnerValueFactory.DoubleSpinnerValueFactory) orderLevel.getValueFactory()).setAmountToStepBy(MathUtils.getSpinnerStep(newValueItem.getNewValue()));
                        newMarketItem.buyValueProperty().removeListener(this);
                    }
                });

                stopOrderLabel.setText(String.format(minPointsPattern, newMarketItem.getMinNormalStopOrLimitDistance()));
                ((SpinnerValueFactory.DoubleSpinnerValueFactory) stopOrder.getValueFactory())
                        .setMin(newMarketItem.getMinNormalStopOrLimitDistance());//TODO Copy/Paste

                guaranteedStop.selectedProperty().addListener((observable1, oldValue, newValue) -> {
                    if (newValue) {
                        ((SpinnerValueFactory.DoubleSpinnerValueFactory) stopOrder.getValueFactory())
                                .setMin(Math.floor(newMarketItem.getBuyValue().getNewValue() * newMarketItem.getScalingFactor() * newMarketItem.getMinControlledRiskStopDistance()));
                        stopOrderLabel.setText(String.format(minPercentPatten, String.valueOf(newMarketItem.getMinControlledRiskStopDistance() * PERCENT)));//TODO Extra zeros
                    } else {
                        stopOrderLabel.setText(String.format(minPointsPattern, String.valueOf(newMarketItem.getMinNormalStopOrLimitDistance())));//Do not use float format %f because it left
                        ((SpinnerValueFactory.DoubleSpinnerValueFactory) stopOrder.getValueFactory())
                                .setMin(newMarketItem.getMinNormalStopOrLimitDistance());//TODO Copy/Paste
                    }
                });

                orderForm.setEpic(newMarketItem.getEpic());
                orderForm.setExpiry(newMarketItem.getExpiry());

                this.marketItem = newMarketItem;
            }
        };
    }

    private CreatePositionResponse sendCreateOrderRequest(OrderForm orderForm, String orderType) {
        try {
            CreatePositionRequest.CreatePositionRequestBuilder createPositionRequestBuilder
                    = CreatePositionRequest.getBuilder();
            createPositionRequestBuilder.setCurrencyCode(orderForm.getCurrencyCode());
            createPositionRequestBuilder.setDirection(CreatePositionRequest.Direction.valueOf(orderType));
            createPositionRequestBuilder.setEpic(orderForm.getEpic());
            createPositionRequestBuilder.setExpiry(orderForm.getExpiry());
            createPositionRequestBuilder.setForceOpen(orderForm.getForceOpen());
            if (StringUtils.isNotEmpty(orderForm.getGoodTillDate())) {
                createPositionRequestBuilder.setTimeInForce(CreatePositionRequest.TimeInForce.GOOD_TILL_DATE);
                createPositionRequestBuilder.setGoodTillDate(orderForm.getGoodTillDate());
            } else createPositionRequestBuilder.setTimeInForce(CreatePositionRequest.TimeInForce.GOOD_TILL_CANCELLED);
            createPositionRequestBuilder.setGuaranteedStop(orderForm.getGuaranteedStop());
            createPositionRequestBuilder.setLevel(BigDecimal.valueOf(orderForm.getOrderLevel()));
            createPositionRequestBuilder.setLimitDistance(BigDecimal.valueOf(orderForm.getLimitDistance()));
            createPositionRequestBuilder.setSize(BigDecimal.valueOf(orderForm.getSize()));
            createPositionRequestBuilder.setStopDistance(BigDecimal.valueOf(orderForm.getStopDistance()));
            createPositionRequestBuilder.setType(CreatePositionRequest.OrderType.valueOf(orderForm.getType().toUpperCase()));
            return traderFacade.createPosition(createPositionRequestBuilder.build());
        } catch (Exception e) {
            logger.error("Unable to create order", e);
            e.printStackTrace();
            showErrorAlert(e.getMessage());
        }
        return null;
    }

    private void showErrorAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resourceManager.getLocalizedString("dialog.new.order.error"));
        alert.setHeaderText(resourceManager.getLocalizedString("dialog.new.order.unable.create.order"));
        alert.setContentText(message);
        alert.show();
    }

    @Override
    public void setDemo(boolean demo) {
        isDemo = demo;
    }

}
