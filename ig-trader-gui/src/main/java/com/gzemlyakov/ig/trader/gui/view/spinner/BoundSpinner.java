package com.gzemlyakov.ig.trader.gui.view.spinner;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Spinner;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class BoundSpinner<T, S> extends Spinner<T> {

    private ObjectProperty<S> bounder;
    private SpinnerValueFactoryResolver<S, T> spinnerValueFactoryResolver;

    public BoundSpinner() {
        super();
        init();
    }

    public BoundSpinner(S bounder, SpinnerValueFactoryResolver<S, T> spinnerValueFactoryResolver) {
        super();
        bounderProperty().set(bounder);
        this.spinnerValueFactoryResolver = spinnerValueFactoryResolver;
        init();
    }

    private void init() {
        bounderProperty().addListener((observable, oldValue, newValue) -> {
            if (spinnerValueFactoryResolver == null) return;
            setValueFactory(spinnerValueFactoryResolver.getSpinnerValueFactory(BoundSpinner.this, newValue));
        });
    }

    public S getBounder() {
        return bounder.get();
    }

    public ObjectProperty<S> bounderProperty() {
        if (bounder == null) {
            bounder = new SimpleObjectProperty<>(this, "object");
        }
        return bounder;
    }

    public void setBounder(S bounder) {
        this.bounder.set(bounder);
    }

    public SpinnerValueFactoryResolver<S, T> getSpinnerValueFactoryResolver() {
        return spinnerValueFactoryResolver;
    }

    public void setSpinnerValueFactoryResolver(SpinnerValueFactoryResolver<S, T> spinnerValueFactoryResolver) {
        this.spinnerValueFactoryResolver = spinnerValueFactoryResolver;
    }
}
