package com.gzemlyakov.ig.trader.gui.presenter.order.error;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface ErrorMessageResolver {

    String resolve(String key);

}
