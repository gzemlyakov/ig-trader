package com.gzemlyakov.ig.trader.gui.view.label;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class FormattedLabel<T, S extends LabelFormatter<T>> extends Label {

    private ObjectProperty<T> objectProperty;
    private S labelFormatter;

    public FormattedLabel() {
        super();
        init();
    }

    public FormattedLabel(T object, S labelFormatter) {
        super();
        objectProperty().set(object);
        this.labelFormatter = labelFormatter;
        setText(labelFormatter.format(object));
        init();
    }

    private void init() {
        objectProperty().addListener((observable, oldValue, newValue) -> {
            if (labelFormatter == null) setText(newValue.toString());
            else setText(labelFormatter.format(newValue));
        });
    }

    public T getObjectProperty() {
        return objectProperty.get();
    }

    public ObjectProperty<T> objectProperty() {
        if (objectProperty == null) {
            objectProperty = new SimpleObjectProperty<>(this, "object");
        }
        return objectProperty;
    }

    public void setObjectProperty(T objectProperty) {
        this.objectProperty.set(objectProperty);
    }

    public S getLabelFormatter() {
        return labelFormatter;
    }

    public void setLabelFormatter(S labelFormatter) {
        this.labelFormatter = labelFormatter;
    }

}
