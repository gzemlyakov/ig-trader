package com.gzemlyakov.ig.trader.gui.presenter.order;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface NewOrderDialogPresenter {
    void init();

    void showNewOrderDialog();

    void setDemo(boolean demo);
}
