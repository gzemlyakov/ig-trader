package com.gzemlyakov.ig.trader.gui.presenter.login;

import com.gzemlyakov.ig.trader.gui.model.credentials.LoginFormModel;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface LoginFormValidator {

    boolean isLoginValid(LoginFormModel loginModel);

    boolean isPasswordValid(LoginFormModel loginModel);

    boolean isApiKeyValid(LoginFormModel loginModel);

}
