package com.gzemlyakov.ig.trader.gui.view.builder.chart;

import com.soilprod.javafx.chart.candlestick.CandleStickChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.util.Builder;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class CandleStickChartBuilder implements Builder<CandleStickChart> {

    private String title;
    private String id;
    private boolean horizontalGridLinesVisible = true;
    private boolean verticalGridLinesVisible = true;

    @Override
    public CandleStickChart build() {
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);
        yAxis.setForceZeroInRange(false);
        CandleStickChart<LocalDateTime, NumberAxis> chart = new CandleStickChart(new CategoryAxis(), yAxis);
        if (StringUtils.isNotEmpty(id)) chart.setId(id);
        chart.setHorizontalGridLinesVisible(horizontalGridLinesVisible);
        chart.setVerticalGridLinesVisible(verticalGridLinesVisible);
        return chart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isHorizontalGridLinesVisible() {
        return horizontalGridLinesVisible;
    }

    public void setHorizontalGridLinesVisible(boolean horizontalGridLinesVisible) {
        this.horizontalGridLinesVisible = horizontalGridLinesVisible;
    }

    public boolean isVerticalGridLinesVisible() {
        return verticalGridLinesVisible;
    }

    public void setVerticalGridLinesVisible(boolean verticalGridLinesVisible) {
        this.verticalGridLinesVisible = verticalGridLinesVisible;
    }

}
