package com.gzemlyakov.ig.trader.gui.model.credentials;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class LoginFormModel {

    private StringProperty logIn = new SimpleStringProperty(this, "logIn");
    private StringProperty password = new SimpleStringProperty(this, "password");
    private StringProperty apiKey = new SimpleStringProperty(this, "apiKey");
    private BooleanProperty rememberNextTime = new SimpleBooleanProperty(this, "rememberNextTime");

    public LoginFormModel() {}

    public String getLogIn() {
        return logIn.get();
    }

    public StringProperty logInProperty() {
        return logIn;
    }

    public void setLogIn(String logIn) {
        this.logIn.set(logIn);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getApiKey() {
        return apiKey.get();
    }

    public StringProperty apiKeyProperty() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey.set(apiKey);
    }

    public boolean getRememberNextTime() {
        return rememberNextTime.get();
    }

    public BooleanProperty rememberNextTimeProperty() {
        return rememberNextTime;
    }

    public void setRememberNextTime(boolean rememberNextTime) {
        this.rememberNextTime.set(rememberNextTime);
    }
}
