package com.gzemlyakov.ig.trader.gui.view.table.market;

import com.gzemlyakov.ig.trader.core.model.market.ValueItem;
import com.gzemlyakov.ig.trader.gui.model.market.MarketItem;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class PriceTableCellFactory implements Callback<TableColumn<MarketItem, ValueItem>, TableCell<MarketItem, ValueItem>> {

    private static final String VALUE_UP_PATTERN = "%s \u25B2";
    private static final String VALUE_EQUAL_PATTERN = "%s \u25A0";
    private static final String VALUE_DOWN_PATTERN = "%s \u25BC";

    @Override
    public TableCell<MarketItem, ValueItem> call(TableColumn<MarketItem, ValueItem> param) {
        return new TableCell<MarketItem, ValueItem>() {

            @Override
            protected void updateItem(ValueItem item, boolean empty) {
                super.updateItem(item, empty);

                if (getTableRow() == null || getTableRow().getItem() == null) return;
                if (item == null || empty) {
                    setText("-");
                    return;
                }
                clearCellStyle(this);
                if (item.getNewValue() > item.getOldValue()) {
                    getStyleClass().add("cell-green");
                    setText(String.format(VALUE_UP_PATTERN, item.getNewValue()));
                } else if (item.getNewValue() == item.getOldValue()) {
                    setText(String.format(VALUE_EQUAL_PATTERN, item.getNewValue()));
                } else {
                    getStyleClass().add("cell-red");
                    setText(String.format(VALUE_DOWN_PATTERN, item.getNewValue()));
                }
            }

            @Override
            protected boolean isItemChanged(ValueItem oldItem, ValueItem newItem) {
                return super.isItemChanged(oldItem, newItem);
            }
        };
    }

    private void clearCellStyle(TableCell cell) {
        cell.getStyleClass().remove("cell-green");
        cell.getStyleClass().remove("cell-red");
    }

}
