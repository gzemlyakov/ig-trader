package com.gzemlyakov.ig.trader.gui.view.resource;

import java.io.IOException;
import java.net.URL;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface ResourceManager {

    String getIcon(String icon) throws IllegalArgumentException, IOException;

    String getStyleSheet(String name) throws IOException;

    URL getFxml(String name) throws IOException;

    String getLocalizedString(String key);

}
