package com.gzemlyakov.ig.trader.gui.view.label;

import com.gzemlyakov.ig.trader.gui.model.market.MarketItem;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class LabelFormatterFactory {

    private static final String currentLevelFormat = "%s / %s";

    public static LabelFormatter<MarketItem> currentLevelFormatter() {
        return marketItem -> {
            String buyValue = marketItem.getBuyValue() == null ? "-"
                    : marketItem.getBuyValue().toString();
            String sellValue = marketItem.getBuyValue() == null ? "-"
                    : marketItem.getSellValue().toString();
            return String.format(currentLevelFormat, buyValue, sellValue);
        };
    }

}
