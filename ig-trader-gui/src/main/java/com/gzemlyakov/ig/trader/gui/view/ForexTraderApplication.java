package com.gzemlyakov.ig.trader.gui.view;

import com.gzemlyakov.ig.trader.gui.presenter.ForexTraderPresenter;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class ForexTraderApplication extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        ForexTraderPresenter forexTraderPresenter = context.getBean(ForexTraderPresenter.class);
        forexTraderPresenter.init(primaryStage);
        forexTraderPresenter.launch();
    }

}
