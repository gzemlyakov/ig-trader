package com.gzemlyakov.ig.trader.gui.presenter.util;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class MathUtils {

    public static double getSpinnerStep(double d) {
        return Math.pow(10d, -countDecimalDigits(d) + 1d);
    }

    public static int countDecimalDigits(double d) {
        String text = Double.toString(Math.abs(d));
        int integerPlaces = text.indexOf('.');
        return text.length() - integerPlaces - 1;
    }

}
