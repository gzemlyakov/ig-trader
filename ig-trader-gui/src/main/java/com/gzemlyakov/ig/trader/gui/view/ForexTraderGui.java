package com.gzemlyakov.ig.trader.gui.view;

import com.gzemlyakov.ig.trader.gui.view.composite.Composite;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface ForexTraderGui extends Composite {

    void init(Stage primaryStage);

    void show();

    void close();

    boolean isShowing();
}
