package com.gzemlyakov.ig.trader.gui.model.chart.datasource;

import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketObserver;
import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.core.model.market.MarketItemUpdateInfo;
import com.gzemlyakov.ig.trader.gui.model.chart.ComboBoxPair;
import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import com.soilprod.javafx.chart.candlestick.CandleStickChart;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ChartDataSource implements MarketObserver {

    private static final long MINUTES_IN_DAY = 24L * 60L;

    private static final Map<String, String> RAW_TIME_FRAMES = new LinkedHashMap<String, String>() {{
        put("1 {0}", IgTimeFrames.MINUTE.name());
        put("2 {1}", IgTimeFrames.MINUTE_2.name());
        put("3 {1}", IgTimeFrames.MINUTE_3.name());
        put("5 {1}", IgTimeFrames.MINUTE_5.name());
        put("10 {1}", IgTimeFrames.MINUTE_10.name());
        put("15 {1}", IgTimeFrames.MINUTE_15.name());
        put("30 {1}", IgTimeFrames.MINUTE_30.name());
        put("1 {2}", IgTimeFrames.HOUR.name());
        put("2 {3}", IgTimeFrames.HOUR_2.name());
        put("3 {3}", IgTimeFrames.HOUR_3.name());
        put("4 {3}", IgTimeFrames.HOUR_4.name());
        put("{4}", IgTimeFrames.DAY.name());
        put("{5}", IgTimeFrames.WEEK.name());
        put("{6}", IgTimeFrames.MONTH.name());
    }};

    private static final Map<String, TemporalAmount> TIME_FRAMES_MAP = new HashMap<String, TemporalAmount>() {{
        put(IgTimeFrames.MINUTE.name(), Duration.ofMinutes(1));
        put(IgTimeFrames.MINUTE_2.name(), Duration.ofMinutes(2));
        put(IgTimeFrames.MINUTE_3.name(), Duration.ofMinutes(3));
        put(IgTimeFrames.MINUTE_5.name(), Duration.ofMinutes(5));
        put(IgTimeFrames.MINUTE_10.name(), Duration.ofMinutes(10));
        put(IgTimeFrames.MINUTE_15.name(), Duration.ofMinutes(15));
        put(IgTimeFrames.MINUTE_30.name(), Duration.ofMinutes(30));
        put(IgTimeFrames.HOUR.name(), Duration.ofHours(1));
        put(IgTimeFrames.HOUR_2.name(), Duration.ofHours(2));
        put(IgTimeFrames.HOUR_3.name(), Duration.ofHours(3));
        put(IgTimeFrames.HOUR_4.name(), Duration.ofHours(4));
        put(IgTimeFrames.DAY.name(), Period.ofDays(1));
        put(IgTimeFrames.WEEK.name(), Period.ofDays(7));
        put(IgTimeFrames.MONTH.name(), Period.ofMonths(1));
    }};

    public static final DateTimeFormatter PRICE_ITEM_FORMATTER
            = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").withZone(ZoneId.of("UTC").normalized());
    public static final DateTimeFormatter TIME_CHART_FORMATTER = DateTimeFormatter.ofPattern("dd MMM HH:mm");
    public static final DateTimeFormatter DATE_CHART_FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy");

    private static final Map<String, DateTimeFormatter> TIME_FRAMES_DATE_FORMATTER = new HashMap<String, DateTimeFormatter>() {{
        put(IgTimeFrames.MINUTE.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_2.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_3.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_5.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_10.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_15.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.MINUTE_30.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.HOUR.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.HOUR_2.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.HOUR_3.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.HOUR_4.name(), TIME_CHART_FORMATTER);
        put(IgTimeFrames.DAY.name(), DATE_CHART_FORMATTER);
        put(IgTimeFrames.WEEK.name(), DATE_CHART_FORMATTER);
        put(IgTimeFrames.MONTH.name(), DATE_CHART_FORMATTER);
    }};

    private final Logger logger = LoggerFactory.getLogger(ChartDataSource.class);

    @Value("${chart.bar.number:26}")
    private int numberOfBars;

    @Autowired
    ResourceManager resourceManager;

    private ObservableList<ComboBoxPair> timeFrames;

    private XYChart.Series<String, Number> series;

    public ChartDataSource() {
        series = new XYChart.Series<>();
    }

    private void initTimeFrames() {
        String[] localizedTimeFrames = {resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.minute"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.template.minutes"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.hour"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.template.hours"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.template.daily"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.template.weekly"),
                resourceManager.getLocalizedString("chart.toolbar.combobox.time.frame.template.monthly")};
        MessageFormat formatter = new MessageFormat("");
        timeFrames = FXCollections.observableArrayList();
        for (String timeFramePattern : RAW_TIME_FRAMES.keySet()) {
            formatter.applyPattern(timeFramePattern);
            ComboBoxPair timeFrame = new ComboBoxPair(RAW_TIME_FRAMES.get(timeFramePattern),
                    formatter.format(localizedTimeFrames));
            timeFrames.add(timeFrame);
        }
    }

    public ObservableList<ComboBoxPair> getTimeFrames() {
        if (timeFrames == null) initTimeFrames();
        return timeFrames;
    }

    public ComboBoxPair getTimeFrame(String key) {
        if (timeFrames == null) initTimeFrames();
        for (ComboBoxPair timeFrame : timeFrames)
            if (timeFrame.getKey().equals(key)) return timeFrame;
        return null;
    }

    public XYChart.Series<String, Number> getSeries() {
        return series;
    }

    public Pair<ZonedDateTime, ZonedDateTime> getDateRange(String timeFrameKey) {
        ZonedDateTime endDate = ZonedDateTime.now(ZoneOffset.UTC);
        TemporalAmount toSubtract = TIME_FRAMES_MAP.get(timeFrameKey);
        ZonedDateTime startDate = toSubtract instanceof Duration ?
                endDate.minus(((Duration) toSubtract).multipliedBy(numberOfBars))
                : endDate.minus(((Period) toSubtract).multipliedBy(numberOfBars));
        return new Pair<>(startDate, endDate);
    }

    public int getNumberOfBars() {
        return numberOfBars;
    }

    public void updateSeries(String timeFrame, List<PriceItem> priceItems) {
        series.getData().clear();
        DateTimeFormatter chartFormatter = TIME_FRAMES_DATE_FORMATTER.get(timeFrame);
        for (PriceItem priceItem : priceItems) {
            XYChart.Data<String, Number> data = priceItemToXYData(chartFormatter, priceItem);
            if (data != null) series.getData().add(data);
        }
    }

    private XYChart.Data<String, Number> priceItemToXYData(DateTimeFormatter chartFormatter, PriceItem priceItem) {
        ZonedDateTime snapshotTime = ZonedDateTime.parse(priceItem.getSnapshotTime(), PRICE_ITEM_FORMATTER);
        return new XYChart.Data<>(chartFormatter.format(snapshotTime), priceItem.getOpenPrice(),
                new CandleStickChart.CandleStickExtraValues<>(priceItem.getClosePrice(), priceItem.getHighPrice(), priceItem.getLowPrice()));
    }

    public static long timeFrameToMinutes(String timeFrameKey) {
        TemporalAmount timeFrame = TIME_FRAMES_MAP.get(timeFrameKey);
        if (timeFrame instanceof Duration) {
            return ((Duration) timeFrame).toMinutes();
        } else {
            return MINUTES_IN_DAY;
        }
    }

    @Override
    public void onUpdate(MarketItemUpdateInfo marketItemUpdateInfo) {
    }

    private enum IgTimeFrames {
        MINUTE, MINUTE_2, MINUTE_3, MINUTE_5, MINUTE_10, MINUTE_15, MINUTE_30, HOUR, HOUR_2, HOUR_3, HOUR_4, DAY, WEEK,
        MONTH
    }

}
