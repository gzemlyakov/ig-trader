package com.gzemlyakov.ig.trader.gui.model.order;

import javafx.beans.property.*;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class OrderForm {

    private ObjectProperty<String> currencyCode = new SimpleObjectProperty<>(this, "currencyCode");
    private ObjectProperty<String> direction = new SimpleObjectProperty<>(this, "direction");
    private StringProperty epic = new SimpleStringProperty(this, "epic");
    private StringProperty expiry = new SimpleStringProperty(this, "expiry");
    private BooleanProperty forceOpen = new SimpleBooleanProperty(this, "forceOpen");
    private StringProperty goodTillDate = new SimpleStringProperty(this, "goodTillDate");
    private BooleanProperty guaranteedStop = new SimpleBooleanProperty(this, "guaranteedStop");
    private ObjectProperty<Double> orderLevel = new SimpleObjectProperty<>(this, "orderLevel");
    private ObjectProperty<Integer> limitDistance = new SimpleObjectProperty<>(this, "limitDistance");
    private ObjectProperty<Double> size = new SimpleObjectProperty<>(this, "size");
    private ObjectProperty<Double> stopDistance = new SimpleObjectProperty<>(this, "stopDistance");
    private StringProperty timeInForce = new SimpleStringProperty(this, "timeInForce");
    private ObjectProperty<String> type = new SimpleObjectProperty<>(this, "type");

    public String getCurrencyCode() {
        return currencyCode.get();
    }

    public ObjectProperty<String> currencyCodeProperty() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode.set(currencyCode);
    }

    public String getDirection() {
        return direction.get();
    }

    public ObjectProperty<String> directionProperty() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction.set(direction);
    }

    public String getEpic() {
        return epic.get();
    }

    public StringProperty epicProperty() {
        return epic;
    }

    public void setEpic(String epic) {
        this.epic.set(epic);
    }

    public String getExpiry() {
        return expiry.get();
    }

    public StringProperty expiryProperty() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry.set(expiry);
    }

    public boolean getForceOpen() {
        return forceOpen.get();
    }

    public BooleanProperty forceOpenProperty() {
        return forceOpen;
    }

    public void setForceOpen(boolean forceOpen) {
        this.forceOpen.set(forceOpen);
    }

    public String getGoodTillDate() {
        return goodTillDate.get();
    }

    public StringProperty goodTillDateProperty() {
        return goodTillDate;
    }

    public void setGoodTillDate(String goodTillDate) {
        this.goodTillDate.set(goodTillDate);
    }

    public boolean getGuaranteedStop() {
        return guaranteedStop.get();
    }

    public BooleanProperty guaranteedStopProperty() {
        return guaranteedStop;
    }

    public void setGuaranteedStop(boolean guaranteedStop) {
        this.guaranteedStop.set(guaranteedStop);
    }

    public Double getOrderLevel() {
        return orderLevel.get();
    }

    public ObjectProperty<Double> orderLevelProperty() {
        return orderLevel;
    }

    public void setOrderLevel(double orderLevel) {
        this.orderLevel.set(orderLevel);
    }

    public int getLimitDistance() {
        return limitDistance.get();
    }

    public ObjectProperty<Integer> limitDistanceProperty() {
        return limitDistance;
    }

    public void setLimitDistance(int limitDistance) {
        this.limitDistance.set(limitDistance);
    }

    public Double getSize() {
        return size.get();
    }

    public ObjectProperty<Double> sizeProperty() {
        return size;
    }

    public void setSize(double size) {
        this.size.set(size);
    }

    public double getStopDistance() {
        return stopDistance.get();
    }

    public ObjectProperty<Double> stopDistanceProperty() {
        return stopDistance;
    }

    public void setStopDistance(double stopDistance) {
        this.stopDistance.set(stopDistance);
    }

    public String getTimeInForce() {
        return timeInForce.get();
    }

    public StringProperty timeInForceProperty() {
        return timeInForce;
    }

    public void setTimeInForce(String timeInForce) {
        this.timeInForce.set(timeInForce);
    }

    public String getType() {
        return type.get();
    }

    public ObjectProperty<String> typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }
}
