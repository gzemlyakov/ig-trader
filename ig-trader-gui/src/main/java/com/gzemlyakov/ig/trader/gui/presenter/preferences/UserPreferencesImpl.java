package com.gzemlyakov.ig.trader.gui.presenter.preferences;

import com.gzemlyakov.ig.trader.gui.model.credentials.IgCredentials;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import java.util.prefs.Preferences;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class UserPreferencesImpl implements UserPreferences {

    private static final String SYMBOL = "symbol";
    private static final String TIME_FRAME = "timeFrame";
    private static final String DEMO = "demo";

    private static final String CREDENTIALS = "credentials";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String API_KEY = "apiKey";

    private Preferences preferences;

    public UserPreferencesImpl() {
        preferences = Preferences.userNodeForPackage(this.getClass());
    }

    @Override
    public void setSymbol(String userName, String symbol) {
        preferences.node(SYMBOL).put(userName, symbol);
    }

    @Override
    public String getSymbol(String userName) {
        return preferences.node(SYMBOL).get(userName, StringUtils.EMPTY);
    }

    @Override
    public void setTimeFrame(String userName, String timeFrame) {
        preferences.node(TIME_FRAME).put(userName, timeFrame);
    }

    @Override
    public String getTimeFrame(String userName) {
        return preferences.node(TIME_FRAME).get(userName, StringUtils.EMPTY);
    }

    @Override
    public void saveCredentials(IgCredentials credentials) {
        preferences.node(CREDENTIALS).put(USERNAME, credentials.getUsername());
        preferences.node(CREDENTIALS).put(PASSWORD, credentials.getPassword());
        preferences.node(CREDENTIALS).put(API_KEY, credentials.getApiKey());
    }

    @Override
    public IgCredentials getCredentials() {
        String username = preferences.node(CREDENTIALS).get(USERNAME, StringUtils.EMPTY);
        String password = preferences.node(CREDENTIALS).get(PASSWORD, StringUtils.EMPTY);
        String apiKey = preferences.node(CREDENTIALS).get(API_KEY, StringUtils.EMPTY);
        return new IgCredentials(username, password, apiKey);
    }

    @Override
    public void clearCredentials() {
        preferences.node(CREDENTIALS).put(USERNAME, StringUtils.EMPTY);
        preferences.node(CREDENTIALS).put(PASSWORD, StringUtils.EMPTY);
        preferences.node(CREDENTIALS).put(API_KEY, StringUtils.EMPTY);
    }

    @Override
    public void setDemo(String userName, boolean isDemo) {
        preferences.node(DEMO).put(userName, String.valueOf(isDemo));
    }

    @Override
    public boolean isDemo(String userName) {
        return Boolean.valueOf(preferences.node(DEMO).get(userName, "false"));
    }

}
