package com.gzemlyakov.ig.trader.gui.view;

import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

import static com.gzemlyakov.ig.trader.gui.view.constant.ErrorMessage.GUI_INIT_BEFORE_SHOW;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ForexTraderGuiImpl implements ForexTraderGui {

    private Stage primaryStage;
    private FXMLLoader fxmlLoader;
    private ResourceManager resourceManager;

    private BorderPane mainPane;

    @Value("${app.title:IG Trader}")
    private String applicationTitle;

    @Value("${scene.width:1024}")
    private double sceneWidth;

    @Value("${scene.height:768}")
    private double sceneHeight;

    @Autowired
    public ForexTraderGuiImpl(FXMLLoader fxmlLoader, ResourceManager resourceManager) {
        this.fxmlLoader = fxmlLoader;
        this.resourceManager = resourceManager;
    }

    @Override
    public void init(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initPrimaryStage(primaryStage);
    }

    private void initPrimaryStage(Stage primaryStage) {
        primaryStage.setTitle(applicationTitle);
        primaryStage.setScene(getMainScene());
        try {
            String iconUrl = resourceManager.getIcon("app-icon.png");
            Image appIcon = new Image(iconUrl);
            primaryStage.getIcons().add(appIcon);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Scene getMainScene() {
        Scene scene = null;
        try {
            URL fxmlUrl = resourceManager.getFxml("ig-trader-gui.fxml");
            fxmlLoader.setLocation(fxmlUrl);
            mainPane = fxmlLoader.load();
            scene = new Scene(mainPane, sceneWidth, sceneHeight);
            scene.getStylesheets().add(resourceManager.getStyleSheet("style.css"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scene;
    }

    @Override
    public void show() {
        if (primaryStage == null) {
            throw new IllegalStateException(GUI_INIT_BEFORE_SHOW.toString());
        }
        primaryStage.show();
    }

    @Override
    public void close() {
        primaryStage.close();
    }

    @Override
    public boolean isShowing() {
        return primaryStage.isShowing();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Node> T lookupChild(String selector) {
        return (T) mainPane.lookup(selector);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Pane> T getMainPane() {
        return (T) mainPane;
    }

}
