package com.gzemlyakov.ig.trader.gui.presenter.order.error;

import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ErrorMessageResolverImpl implements ErrorMessageResolver {

    @Autowired
    ResourceManager resourceManager;

    private static final String DEFAULT_ERROR_KEY = "UNKNOWN";

    @Override
    public String resolve(String key) {
        key = ERROR_MESSAGES.containsKey(key) ? key : DEFAULT_ERROR_KEY;
        return resourceManager.getLocalizedString(ERROR_MESSAGES.get(key));
    }

    private static final Map<String, String> ERROR_MESSAGES = new HashMap<String, String>(){{
        put("ACCOUNT_NOT_ENABLED_TO_TRADING", "dialog.new.order.account.not.enabled.to.trade");
        put("ATTACHED_ORDER_LEVEL_ERROR", "dialog.new.order.attached.order.level.error");
        put("ATTACHED_ORDER_TRAILING_STOP_ERROR", "dialog.new.order.attached.trailing.stop.error");
        put("CANNOT_CHANGE_STOP_TYPE", "dialog.new.order.cannot.change.stop.type");
        put("CANNOT_REMOVE_STOP", "dialog.new.order.cannot.remove.stop");
        put("CLOSING_ONLY_TRADES_ACCEPTED_ON_THIS_MARKET", "dialog.new.order.closing.only.trades");
        put("CONFLICTING_ORDER", "dialog.new.order.conflicting.order");
        put("CR_SPACING", "dialog.new.order.cr.spacing");
        put("DUPLICATE_ORDER_ERROR", "dialog.new.order.duplicate.order");
        put("EXCHANGE_MANUAL_OVERRIDE", "dialog.new.order.exchange.manual.override");
        put("FINANCE_REPEAT_DEALING", "dialog.new.order.finance.repeat.dealing");
        put("FORCE_OPEN_ON_SAME_MARKET_DIFFERENT_CURRENCY", "dialog.new.order.force.open.different.currency");
        put("GENERAL_ERROR", "dialog.new.order.general.error");
        put("GOOD_TILL_DATE_IN_THE_PAST", "dialog.new.order.good.till.date.past");
        put("INSTRUMENT_NOT_FOUND", "dialog.new.order.instrument.not.found");
        put("INSUFFICIENT_FUNDS", "dialog.new.order.insufficient.funds");
        put("LEVEL_TOLERANCE_ERROR", "dialog.new.order.level.tolerance.error");
        put("MANUAL_ORDER_TIMEOUT", "dialog.new.order.manual.order.timeout");
        put("MARKET_CLOSED", "dialog.new.order.market.closed");
        put("MARKET_CLOSED_WITH_EDITS", "dialog.new.order.market.closed.with.edits");
        put("MARKET_CLOSING", "dialog.new.order.market.closing");
        put("MARKET_NOT_BORROWABLE", "dialog.new.order.market.not.borrowable");
        put("MARKET_OFFLINE", "dialog.new.order.market.offline");
        put("MARKET_PHONE_ONLY", "dialog.new.order.market.phone.only");
        put("MARKET_ROLLED", "dialog.new.order.market.rolled");
        put("MARKET_UNAVAILABLE_TO_CLIENT", "dialog.new.order.market.unavailable.to.client");
        put("MAX_AUTO_SIZE_EXCEEDED", "dialog.new.order.max.auto.size.exceeded");
        put("MINIMUM_ORDER_SIZE_ERROR", "dialog.new.order.minimum.order.size.error");
        put("MOVE_AWAY_ONLY_LIMIT", "dialog.new.order.move.away.only.limit");
        put("MOVE_AWAY_ONLY_STOP", "dialog.new.order.move.away.only.stop");
        put("MOVE_AWAY_ONLY_TRIGGER_LEVEL", "dialog.new.order.move.away.only.trigger.level");
        put("OPPOSING_DIRECTION_ORDERS_NOT_ALLOWED", "dialog.new.order.opposing.direction.orders.not.allowed");
        put("OPPOSING_POSITIONS_NOT_ALLOWED", "dialog.new.order.opposing.positions.not.allowed");
        put("ORDER_LOCKED", "dialog.new.order.order.locked");
        put("ORDER_NOT_FOUND", "dialog.new.order.order.not.found");
        put("OVER_NORMAL_MARKET_SIZE", "dialog.new.order.over.normal.market.size");
        put("PARTIALY_CLOSED_POSITION_NOT_DELETED", "dialog.new.order.partialy.closed.position.not.deleted");
        put("POSITION_NOT_AVAILABLE_TO_CLOSE", "dialog.new.order.position.not.available.to.close");
        put("POSITION_NOT_FOUND", "dialog.new.order.position.not.found");
        put("REJECT_SPREADBET_ORDER_ON_CFD_ACCOUNT", "dialog.new.order.reject.spreadbet.order.on.cfd.account");
        put("SIZE_INCREMENT", "dialog.new.order.size.increment");
        put("SPRINT_MARKET_EXPIRY_AFTER_MARKET_CLOSE", "dialog.new.order.sprint.market.expiry.after.market.close");
        put("STOP_OR_LIMIT_NOT_ALLOWED", "dialog.new.order.stop.or.limit.not.allowed");
        put("STOP_REQUIRED_ERROR", "dialog.new.order.stop.required.error");
        put("STRIKE_LEVEL_TOLERANCE", "dialog.new.order.strike.level.tolerance");
        put("SUCCESS", "dialog.new.order.success");
        put("TRAILING_STOP_NOT_ALLOWED", "dialog.new.order.trailing.stop.not.allowed");
        put("UNKNOWN", "dialog.new.order.unknown");
        put("WRONG_SIDE_OF_MARKET", "dialog.new.order.wrong.side.of.market");
    }};

}
