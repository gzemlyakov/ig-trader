package com.gzemlyakov.ig.trader.gui.model.credentials;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class IgCredentials {

    private String username;
    private String password;
    private String apiKey;

    public IgCredentials(String username, String password, String apiKey) {
        this.username = username;
        this.password = password;
        this.apiKey = apiKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

}
