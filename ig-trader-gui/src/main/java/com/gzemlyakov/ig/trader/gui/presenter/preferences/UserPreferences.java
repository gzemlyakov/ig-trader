package com.gzemlyakov.ig.trader.gui.presenter.preferences;

import com.gzemlyakov.ig.trader.gui.model.credentials.IgCredentials;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface UserPreferences {

    void setSymbol(String userName, String symbol);

    String getSymbol(String userName);

    void setTimeFrame(String userName, String timeFrame);

    String getTimeFrame(String userName);

    void saveCredentials(IgCredentials igCredentials);

    IgCredentials getCredentials();

    void clearCredentials();

    void setDemo(String userName, boolean isDemo);

    boolean isDemo(String userName);

}
