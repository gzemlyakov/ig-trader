package com.gzemlyakov.ig.trader.gui.presenter.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public class DateUtils {

    private static final TimeZone UTC = TimeZone.getTimeZone("UTC");
    private static final DateFormat utcFormat = new SimpleDateFormat("yyyy/mm/dd hh:mm:ss");

    public static GregorianCalendar stringToGregorianCalendar(String date, DateFormat dateFormat)
            throws ParseException {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(dateFormat.parse(date));
        return calendar;
    }

    public static String toUtcString(Calendar calendar) {
        calendar.setTimeZone(UTC);
        return utcFormat.format(calendar.getTime());
    }

}
