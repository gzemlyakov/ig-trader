package com.gzemlyakov.ig.trader.gui.view.dialog;

import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URL;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public abstract class AbstractDialog<R> extends Dialog<R> implements CustomDialog {

    protected ResourceManager resourceManager;
    protected FXMLLoader fxmlLoader;

    protected Pane mainPane;

    public AbstractDialog(ResourceManager resourceManager, FXMLLoader fxmlLoader) {
        this.resourceManager = resourceManager;
        this.fxmlLoader = fxmlLoader;
        init();
    }

    public void init() {
        try {
            URL fxmlUrl = resourceManager.getFxml(getFxmlName());
            fxmlLoader.setLocation(fxmlUrl);
            mainPane = fxmlLoader.load();
            this.getDialogPane().setContent(mainPane);
            if (StringUtils.isNotEmpty(getStyleSheetName()))
                this.getDialogPane().getStylesheets().add(resourceManager.getStyleSheet(getStyleSheetName()));
            if (StringUtils.isNotEmpty(getDialogPaneStyleClass()))
                this.getDialogPane().getStyleClass().add(getDialogPaneStyleClass());
            if (StringUtils.isNotEmpty(getDialogTitle()))
                this.setTitle(getDialogTitle());
            if (StringUtils.isNotEmpty(getDialogHeaderText()))
                this.setHeaderText(getDialogHeaderText());
            if (ArrayUtils.isNotEmpty(getDialogButtons()))
                this.getDialogPane().getButtonTypes().addAll(getDialogButtons());
            initDialogSize();
            initDialogIcon();
            hideButtonBar();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void initDialogSize() {
        if (getDialogWidth() != null) this.getDialogPane().setPrefWidth(getDialogWidth());
        if (getDialogHeight() != null) this.getDialogPane().setPrefWidth(getDialogHeight());
    }

    protected abstract Double getDialogWidth();

    protected abstract Double getDialogHeight();

    private void initDialogIcon() throws IOException {
        Image dialogIcon = new Image(resourceManager.getIcon(getIconName()));
        ((Stage) this.getDialogPane().getScene().getWindow()).getIcons().add(dialogIcon);
    }

    protected void hideButtonBar() {
        ButtonBar buttonBar = (ButtonBar) this.getDialogPane().lookup(".button-bar");
        buttonBar.setVisible(false);
        buttonBar.setPrefHeight(0d);
    }

    protected abstract String getFxmlName();

    protected String getStyleSheetName() {
        return "style.css";
    }

    protected String getDialogPaneStyleClass() {
        return "main-pane";
    }

    protected abstract String getDialogTitle();

    protected abstract String getDialogHeaderText();

    protected abstract String getIconName();

    protected ButtonType[] getDialogButtons() {
        ButtonType cancelStub = new ButtonType("", ButtonBar.ButtonData.CANCEL_CLOSE);//TODO Hack. Unable to apply different style to dialog buttons. Unable to close dialog if there is no buttons
        return new ButtonType[] {cancelStub};
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Node> T lookupChild(String selector) {
        return (T) mainPane.lookup(selector);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Pane> T getMainPane() {
        return (T) mainPane;
    }

}
