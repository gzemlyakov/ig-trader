package com.gzemlyakov.ig.trader.gui.model.market;

import com.gzemlyakov.ig.trader.gui.model.order.OrderItem;
import javafx.collections.ObservableList;

import java.util.List;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
public interface ForexMarket {

/*
    void init(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> marketItems);
*/

    void init(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> symbols,
              List<com.gzemlyakov.ig.trader.core.model.order.OrderItem> openOrders);

    void initSymbols(List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> symbols);

    void initOpenOrders(List<com.gzemlyakov.ig.trader.core.model.order.OrderItem> openOrders);

    ObservableList<MarketItem> getMarketItems();

    MarketItem getMarketItemByEpic(String epic);

    ObservableList<OrderItem> getOrderItems();
}
