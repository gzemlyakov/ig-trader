package com.gzemlyakov.ig.trader.gui.presenter;

import com.gzemlyakov.ig.trader.core.TraderFacade;
import com.gzemlyakov.ig.trader.core.iggroup.data.market.MarketObserver;
import com.gzemlyakov.ig.trader.core.model.chart.PriceItem;
import com.gzemlyakov.ig.trader.gui.model.chart.ComboBoxPair;
import com.gzemlyakov.ig.trader.gui.model.chart.datasource.ChartDataSource;
import com.gzemlyakov.ig.trader.gui.model.credentials.IgCredentials;
import com.gzemlyakov.ig.trader.gui.model.credentials.LoginFormModel;
import com.gzemlyakov.ig.trader.gui.model.market.ForexMarket;
import com.gzemlyakov.ig.trader.gui.model.market.MarketItem;
import com.gzemlyakov.ig.trader.gui.model.order.DemoOrderDataSource;
import com.gzemlyakov.ig.trader.gui.model.order.OrderItem;
import com.gzemlyakov.ig.trader.gui.presenter.login.LoginFormValidator;
import com.gzemlyakov.ig.trader.gui.presenter.order.NewOrderDialogPresenter;
import com.gzemlyakov.ig.trader.gui.presenter.os.OsManager;
import com.gzemlyakov.ig.trader.gui.presenter.preferences.UserPreferences;
import com.gzemlyakov.ig.trader.gui.view.ForexTraderGui;
import com.gzemlyakov.ig.trader.gui.view.dialog.CustomDialog;
import com.gzemlyakov.ig.trader.gui.view.dialog.order.NewOrderDialog;
import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import com.soilprod.javafx.chart.candlestick.CandleStickChart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.controlsfx.control.ToggleSwitch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class ForexTraderPresenterImpl implements ForexTraderPresenter {

    private final Logger logger = LoggerFactory.getLogger(ForexTraderPresenterImpl.class);

    private ForexTraderGui view;
    private CustomDialog loginDialog;
    private NewOrderDialogPresenter newOrderDialogPresenter;
    private TraderFacade traderFacade;

    @Autowired
    private OsManager osManager;

    @Autowired
    private LoginFormValidator loginFormValidator;

    @Autowired
    private ResourceManager resourceManager;

    @Autowired
    private ForexMarket forexMarket;

    @Autowired
    private ChartDataSource chartDataSource;

    @Autowired
    private UserPreferences userPreferences;

    @Autowired
    private DemoOrderDataSource demoOrderDataSource;

    @Autowired
    private NewOrderDialog newOrderDialog;

    @Value("${lost.details.page:https://www.ig.com/de/lost-details}")
    private String lostDetailsPage;

    @Value("${login.error.separator:; }")
    private String loginErrorSeparator;

    private ScheduledService<List<PriceItem>> chartUpdateService;

    private String userName;

    @Autowired
    public ForexTraderPresenterImpl(ForexTraderGui view, NewOrderDialogPresenter newOrderDialogPresenter, CustomDialog loginDialog,
                                    TraderFacade traderFacade) {
        this.loginDialog = loginDialog;
        this.view = view;
        this.newOrderDialogPresenter = newOrderDialogPresenter;
        this.traderFacade = traderFacade;
    }

    @Override
    public void init(Stage primaryStage) {
        view.init(primaryStage);

        primaryStage.setOnCloseRequest(event -> {
            try {
                traderFacade.disconnect();
                savePreferences();
            } catch (Exception e) {
                logger.warn("Disconnect from ig.com service failed: " + e.getMessage(), e);
            }
        });
    }

    private void savePreferences() {
        if (!view.isShowing()) return;
        String symbolToSave = view.<ComboBox<MarketItem>>lookupChild("#symbol-chooser").getValue().getEpic();
        String timeFrameToSave = view.<ComboBox<ComboBoxPair>>lookupChild("#time-frame-chooser").getValue().getKey();
        boolean isDemo = view.<ToggleSwitch>lookupChild("#demo-switch").isSelected();
        userPreferences.setSymbol(userName, symbolToSave);
        userPreferences.setTimeFrame(userName, timeFrameToSave);
        userPreferences.setDemo(userName, isDemo);
    }

    public void launch() {
        IgCredentials credentials = userPreferences.getCredentials();
        if (!isCredentialsEmpty(credentials)) {
            try {
                traderFacade.connect(credentials.getUsername(), credentials.getPassword(), credentials.getApiKey());
                this.userName = credentials.getUsername(); //TODO Store username in preferences
                onLogIn();
            } catch (Exception e) {
                logger.error("Unable to connect with saved credentials", e);
                e.printStackTrace();
                initLoginDialogAndShow();
            }
        } else initLoginDialogAndShow();
    }

    private boolean isCredentialsEmpty(IgCredentials credentials) {
        return StringUtils.isEmpty(credentials.getUsername()) || StringUtils.isEmpty(credentials.getPassword())
                || StringUtils.isEmpty(credentials.getApiKey());
    }

    private void initLoginDialogAndShow() {
        initLoginDialog();
        loginDialog.show();
    }

    private void initLoginDialog() {
        LoginFormModel loginFormModel = bindLoginDialogProps();
        attachLoginDialogEvents(loginFormModel);
    }

    private LoginFormModel bindLoginDialogProps() {
        LoginFormModel loginFormModel = new LoginFormModel();
        loginFormModel.logInProperty().bind(loginDialog.<TextField>lookupChild("#login-username-input").textProperty());
        loginFormModel.passwordProperty().bind(loginDialog.<PasswordField>lookupChild("#login-password-input").textProperty());
        loginFormModel.apiKeyProperty().bind(loginDialog.<TextField>lookupChild("#login-api-input").textProperty());
        loginFormModel.rememberNextTimeProperty().bind(loginDialog.<CheckBox>lookupChild("#login-remember-check-box").selectedProperty());
        return loginFormModel;
    }

    private void attachLoginDialogEvents(LoginFormModel loginFormModel) {
        loginDialog.<Hyperlink>lookupChild("#forgot-password-link").setOnAction(event -> {
            try {
                if (osManager != null) osManager.browsePage(new URI(lostDetailsPage));
            } catch (IOException e) {
                logger.warn("Unable to browse page " + lostDetailsPage, e);
            } catch (URISyntaxException e) {
                logger.warn(String.format("Unable to parse string %s as a URI reference", lostDetailsPage), e);
            }
        });
        loginDialog.<Button>lookupChild("#login-button").setOnAction(event -> {
            try {
                if (!validateLoginForm(loginFormModel)) return;
                traderFacade.connect(loginFormModel.getLogIn(), loginFormModel.getPassword(),
                        loginFormModel.getApiKey());
                if (loginFormModel.getRememberNextTime()) {
                    userPreferences.saveCredentials(new IgCredentials(loginFormModel.getLogIn(), loginFormModel.getPassword(),
                            loginFormModel.getApiKey()));
                } else {
                    userPreferences.clearCredentials();
                }
                this.userName = loginFormModel.getLogIn();
                onLogIn();
            } catch (Exception e) {
                Label errorMessageHolder = loginDialog.<Label>lookupChild("#login-error-message-holder");
                if (isLoginException(e)) {
                    errorMessageHolder.setText(
                            resourceManager.getLocalizedString("dialog.login.error.invalid.credentials"));
                    markAllLoginInputs();
                } else {
                    errorMessageHolder.setText(resourceManager.getLocalizedString("dialog.login.unexpected.error")
                            + e.getLocalizedMessage());
                }
                logger.warn("Unable to connect to ig.com api " + e.getMessage(), e);
                e.printStackTrace();
            }
        });
    }

    private boolean validateLoginForm(LoginFormModel loginFormModel) {
        cleanUpLoginInputMarks();
        List<String> errorMessages = new ArrayList<>();
        List<String> incorrectInputSelectors = new ArrayList<>();
        if (!loginFormValidator.isLoginValid(loginFormModel)) {
            incorrectInputSelectors.add("#login-input-wrapper");
            errorMessages.add(resourceManager.getLocalizedString("dialog.login.error.empty.login"));
        }
        if (!loginFormValidator.isPasswordValid(loginFormModel)) {
            incorrectInputSelectors.add("#password-input-wrapper");
            errorMessages.add(resourceManager.getLocalizedString("dialog.login.error.empty.password"));
        }
        if (!loginFormValidator.isApiKeyValid(loginFormModel)) {
            incorrectInputSelectors.add("#api-input-wrapper");
            errorMessages.add(resourceManager.getLocalizedString("dialog.login.error.empty.api.key"));
        }
        if (errorMessages.isEmpty()) return true;
        Label errorMessageHolder = loginDialog.<Label>lookupChild("#login-error-message-holder");
        errorMessageHolder.setText(StringUtils.join(errorMessages, loginErrorSeparator));
        markLoginInputs(incorrectInputSelectors.toArray(new String[incorrectInputSelectors.size()]));
        return false;
    }

    private void cleanUpLoginInputMarks() {
        HBox loginInputWrapper = loginDialog.<HBox>lookupChild("#login-input-wrapper");
        loginInputWrapper.getStyleClass().remove("input-wrapper-incorrect");
        HBox passwordInputWrapper = loginDialog.<HBox>lookupChild("#password-input-wrapper");
        passwordInputWrapper.getStyleClass().remove("input-wrapper-incorrect");
        HBox apiInputWrapper = loginDialog.<HBox>lookupChild("#api-input-wrapper");
        apiInputWrapper.getStyleClass().remove("input-wrapper-incorrect");
    }

    private boolean isLoginException(Exception e) {
        return e instanceof HttpClientErrorException
                && (HttpStatus.FORBIDDEN.equals(((HttpClientErrorException) e).getStatusCode())
                || HttpStatus.UNAUTHORIZED.equals(((HttpClientErrorException) e).getStatusCode()));
    }

    private void markAllLoginInputs() {
        markLoginInputs("#login-input-wrapper", "#password-input-wrapper", "#api-input-wrapper");
    }

    private void markLoginInputs(String... inputSelectors) {
        for (String inputSelector : inputSelectors) {
            HBox inputWrapper = loginDialog.<HBox>lookupChild(inputSelector);
            inputWrapper.getStyleClass().add("input-wrapper-incorrect");
        }
    }

    private void onLogIn() {
        try {
            loginDialog.hide();
            init();
            view.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        List<com.gzemlyakov.ig.trader.core.model.market.MarketItem> forexMarketItems = traderFacade.initForexMarket();
        forexMarket.init(forexMarketItems, traderFacade.getWorkingOrders());
        initWatchMarketTable(forexMarket.getMarketItems());
        traderFacade.registerMarketObserver((MarketObserver) forexMarket);
        initToolbar();
        initChart();
        initNewOrderDialog();
        redrawChart();
        if (userPreferences.isDemo(userName)) initOrderItemTable(demoOrderDataSource.getOrderItems());
        else initOrderItemTable(forexMarket.getOrderItems());
        attachMainPaneEvents();
    }

    private void initWatchMarketTable(ObservableList<MarketItem> marketItems) {
        TableView<MarketItem> watchMarketTable = view.<TableView<MarketItem>>lookupChild("#market-watch-table");
        watchMarketTable.setItems(marketItems);
    }

    private void initToolbar() {
        view.<ToggleSwitch>lookupChild("#demo-switch").setSelected(userPreferences.isDemo(userName));
    }

    private void initChart() {
        CandleStickChart<String, Number> candleStickChart = view.lookupChild("#chart");
        ObservableList<XYChart.Series<String, Number>> chartData = candleStickChart.getData();
        if (chartData == null) {
            chartData = FXCollections.observableArrayList(chartDataSource.getSeries());
            candleStickChart.setData(chartData);
        } else {
            candleStickChart.getData().add(chartDataSource.getSeries());
        }
        initChartToolbar(forexMarket.getMarketItems());
    }

    private void initChartToolbar(ObservableList<MarketItem> marketItems) {
        initSymbolChooser(marketItems);
        initTimeFrameChooser(userName);
    }

    private void initSymbolChooser(ObservableList<MarketItem> marketItems) {
        ComboBox<MarketItem> symbolChooser = view.lookupChild("#symbol-chooser");
        symbolChooser.setItems(marketItems);
        String savedSymbol = userPreferences.getSymbol(userName);
        MarketItem savedMarketItem = forexMarket.getMarketItemByEpic(savedSymbol);
        if (StringUtils.isNotEmpty(savedSymbol) && savedMarketItem != null)
            symbolChooser.setValue(savedMarketItem);
        else symbolChooser.setValue(marketItems.get(0));
        symbolChooser.setOnAction(event -> redrawChart());
    }

    private void initTimeFrameChooser(String userName) {
        ComboBox<ComboBoxPair> timeFrameChooser = view.lookupChild("#time-frame-chooser");
        ObservableList<ComboBoxPair> timeFrames = chartDataSource.getTimeFrames();
        timeFrameChooser.setItems(timeFrames);
        String savedTimeFrameKey = userPreferences.getTimeFrame(userName);
        ComboBoxPair savedTimeFrame = chartDataSource.getTimeFrame(savedTimeFrameKey);
        if (StringUtils.isNotEmpty(savedTimeFrameKey) && savedTimeFrame != null)
            timeFrameChooser.setValue(savedTimeFrame);
        else timeFrameChooser.setValue(timeFrames.get(0));
        timeFrameChooser.setOnAction(event -> redrawChart());
    }

    private void initNewOrderDialog() {
        newOrderDialogPresenter.init();
        newOrderDialog.setOnHiding(event -> {
            try {
                forexMarket.initOpenOrders(traderFacade.getWorkingOrders());
            } catch (Exception e) {
                logger.error("Unable to retrieve Open Orders data", e);
                e.printStackTrace();
            }
        });
    }

    private void redrawChart() {
        try {
            ComboBox<MarketItem> symbolChooser = view.lookupChild("#symbol-chooser");
            ComboBox<ComboBoxPair> timeFrameChooser = view.<ComboBox<ComboBoxPair>>lookupChild("#time-frame-chooser");
            String epic = symbolChooser.getValue().getEpic();
            String timeFrameKey = timeFrameChooser.getValue().getKey();
            List<PriceItem> priceItems
                    = traderFacade.requestMarketItemHistory(epic, timeFrameKey, chartDataSource.getNumberOfBars());
            chartDataSource.updateSeries(timeFrameKey, priceItems);


            if (chartUpdateService != null) chartUpdateService.cancel();
            chartUpdateService = new ScheduledService<List<PriceItem>>() {
                @Override
                protected Task<List<PriceItem>> createTask() {
                    return new Task<List<PriceItem>>() {
                        @Override
                        protected List<PriceItem> call() throws Exception {
                            return traderFacade.requestMarketItemHistory(epic, timeFrameKey, chartDataSource.getNumberOfBars());
                        }
                    };
                }
            };
            long updatePeriod = chartDataSource.timeFrameToMinutes(timeFrameKey);
            chartUpdateService.setDelay(Duration.minutes(updatePeriod));
            chartUpdateService.setPeriod(Duration.minutes(updatePeriod));
            chartUpdateService.setOnSucceeded(event -> chartDataSource.updateSeries(timeFrameKey, chartUpdateService.getValue()));
            chartUpdateService.start();
        } catch (Exception e) {
            logger.error("Unable to redraw chart", e);
            e.printStackTrace();
        }
    }

    private void attachMainPaneEvents() {
        view.<Button>lookupChild("#new-order-button").setOnAction(event -> newOrderDialogPresenter.showNewOrderDialog());
        view.<Button>lookupChild("#log-out-button").setOnAction(event -> {
            try {
                traderFacade.disconnect();
                userPreferences.clearCredentials();
                view.close();
                initLoginDialogAndShow();
            } catch (Exception e) {
                logger.error("Unable to log out", e);
                e.printStackTrace();
            }
        });
        view.<ToggleSwitch>lookupChild("#demo-switch").selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                TableView<OrderItem> openOrdersTable = view.<TableView<OrderItem>>lookupChild("#open-orders-table");
                if (newValue) {
//                    openOrdersTable.getItems().clear();
                    openOrdersTable.setItems(demoOrderDataSource.getOrderItems());
                } else {
//                    openOrdersTable.getItems().clear();
                    openOrdersTable.setItems(forexMarket.getOrderItems());
                }
                newOrderDialogPresenter.setDemo(newValue);
            }
        });
    }

    private void initOrderItemTable(ObservableList<OrderItem> orderItems) {
        TableView<OrderItem> openOrdersTable = view.<TableView<OrderItem>>lookupChild("#open-orders-table");
        openOrdersTable.setItems(orderItems);
    }

    @Override
    public ForexTraderGui getView() {
        return view;
    }

}
