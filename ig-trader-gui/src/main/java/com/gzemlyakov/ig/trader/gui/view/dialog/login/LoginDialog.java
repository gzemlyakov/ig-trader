package com.gzemlyakov.ig.trader.gui.view.dialog.login;

import com.gzemlyakov.ig.trader.gui.view.dialog.AbstractDialog;
import com.gzemlyakov.ig.trader.gui.view.resource.ResourceManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by gzemlyakov.
 * gzemlyakov@gmail.com
 */
@Component
public class LoginDialog extends AbstractDialog {

    @Autowired
    public LoginDialog(ResourceManager resourceManager, FXMLLoader fxmlLoader) {
        super(resourceManager, fxmlLoader);
    }

    @Override
    public void init() {
        super.init();
        attachInputEvents();
    }

    @Override
    protected Double getDialogWidth() {
        return 400d;
    }

    @Override
    protected Double getDialogHeight() {
        return null;
    }

    private void attachInputEvents() {
        requestFocusOnClick("#login-input-wrapper", "#login-username-input");
        requestFocusOnClick("#password-input-wrapper", "#login-password-input");
        requestFocusOnClick("#api-input-wrapper", "#login-api-input");
    }

    private void requestFocusOnClick(String source, String target) {
        Node focusTarget = mainPane.lookup(target);
        Node clickSource = mainPane.lookup(source);
        clickSource.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> focusTarget.requestFocus());
    }

    @Override
    protected String getFxmlName() {
        return "dialog/login/login-dialog.fxml";
    }

    @Override
    protected String getDialogTitle() {
        return resourceManager.getLocalizedString("dialog.login");
    }

    @Override
    protected String getDialogHeaderText() {
        return resourceManager.getLocalizedString("dialog.login.header.text");
    }

    @Override
    protected String getIconName() {
        return "app-icon.png";
    }

    @Override
    protected ButtonType[] getDialogButtons() {
        ButtonType logIn = new ButtonType(resourceManager.getLocalizedString("dialog.login.button.login"),
                ButtonBar.ButtonData.CANCEL_CLOSE);
        return new ButtonType[]{logIn};
    }

}
